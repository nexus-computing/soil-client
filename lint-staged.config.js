module.exports = {
	'*.{js,ts,vue}': ['vue-cli-service lint', () => 'vue-cli-service test:unit'],
	'*.{ts,vue}': () => 'tsc -p tsconfig.json --noEmit',
};
