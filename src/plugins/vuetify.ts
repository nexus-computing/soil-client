import Vuetify from 'vuetify';
import Vue from 'vue';

Vue.use(Vuetify);

export default new Vuetify({
	breakpoint: {
		thresholds: {
			sm: 768,
			md: 840,
			lg: 1366,
		},
	},
});
