/* eslint-disable @typescript-eslint/no-explicit-any */
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import {
	locations as mockLocations,
	samplingCollection as mockSamplingCollection,
	userLocation as mockUserLocation,
	userLocation,
} from '@/../tests/mockData';
import CollectSamplingButton from '@/components/collect/CollectSamplingButton.vue';

describe('CollectSamplingButton.vue', () => {
	let vm: any;
	let actions: any;

	beforeEach(() => {
		const localVue = createLocalVue();
		localVue.use(Vuex);

		actions = {
			'samplings/setRecord': jest.fn(),
			'samples/setRecords': jest.fn(),
		};
		const getters = {
			getSamplingCollectionStatusBySamplingCollectionId: () => jest.fn(),
		};
		const store = new Vuex.Store({ actions, getters });
		const wrapper = shallowMount(CollectSamplingButton, {
			localVue,
			store,
			propsData: {
				location: mockLocations[0],
				userLocation: mockUserLocation,
				samplingCollectionId: mockSamplingCollection.id,
				samplingCollection: { ...mockSamplingCollection, members: [] },
			},
		});

		const samplingCreateStub = jest.fn();
		const samplingDeleteStub = jest.fn();
		wrapper.vm.$on('samplingCreate', samplingCreateStub);
		wrapper.vm.$on('samplingDelete', samplingDeleteStub);
		vm = wrapper.vm;
	});

	test('creates sampling', () => {
		vm.onSamplingCreate();
		expect(vm.dialogIsVisible).toBe(true);
		expect(vm.currentSampling.geometry.coordinates[0]).toBe(mockUserLocation.coords.longitude);
		expect(vm.currentSampling.geometry.coordinates[1]).toBe(mockUserLocation.coords.latitude);
		expect(vm.currentSampling.properties.geoAccuracy).toBe(mockUserLocation.coords.accuracy);
		expect(vm.currentSampling.soDepth).toBe(mockSamplingCollection.soDepth);
	});

	test('cancels sampling', () => {
		vm.onSamplingCreate();
		expect(vm.dialogIsVisible).toBe(true);
		expect(vm.currentSampling).toBeTruthy();
		vm.closeSamplingDialog();

		expect(vm.dialogIsVisible).toBe(false);
		expect(vm.currentSampling).toBeFalsy();
		expect(vm.currentSamples.length).toBe(0);
	});

	test('saves sampling', () => {
		vm.onSamplingCreate();
		expect(vm.dialogIsVisible).toBe(true);
		expect(vm.currentSampling).toBeTruthy();

		vm.onSamplingSave();
		expect(actions['samplings/setRecord']).toHaveBeenCalled();
		expect(actions['samples/setRecords']).toHaveBeenCalled();
		expect(actions['samplings/setRecord'].mock.calls[0][1].geometry.coordinates[0]).toBe(userLocation.coords.longitude);
		expect(actions['samplings/setRecord'].mock.calls[0][1].geometry.coordinates[1]).toBe(userLocation.coords.latitude);
	});
});
