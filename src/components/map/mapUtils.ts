import { SetupContext } from '@vue/composition-api';
import MapboxDraw from '@mapbox/mapbox-gl-draw';
import { Feature, FeatureCollection, Point, Polygon } from 'geojson';
import camelCase from 'lodash/camelCase';
import { AnyLayout, AnyPaint, EventData, Layer } from 'mapbox-gl';

export enum LayerTypes {
	Locations = 'LOCATIONS',
	ActiveLocation = 'ACTIVE_LOCATION',
	FieldBoundary = 'FIELD_BOUNDARY',
	ActiveFieldBoundary = 'ACTIVE_FIELD_BOUNDARY',
	FieldPolygon = 'FIELD_POLYGON',
	ActiveFieldPolygon = 'ACTIVE_FIELD_POLYGON',
	Markers = 'MARKERS',
	ActiveMarkers = 'ACTIVE_MARKERS',
}

const exposeDrawHandler = (type: MapboxDraw.DrawEventType) => (emit: SetupContext['emit']) => ({
	type,
	handler: (ev: EventData) => {
		const action = camelCase(type).replaceAll(/change/gi, 'Change');
		emit(action, ev);
	},
});

// emits 'drawCreate'
export const drawCreateHandler = exposeDrawHandler('draw.create');
// emits 'drawDelete'
export const drawDeleteHandler = exposeDrawHandler('draw.delete');
// emits 'drawUpdate'
export const drawUpdateHandler = exposeDrawHandler('draw.update');
// emits 'drawRender'
export const drawRenderHandler = exposeDrawHandler('draw.render');
// emits 'drawCombine'
export const drawCombineHandler = exposeDrawHandler('draw.combine');
// emits 'drawUncombine'
export const drawUncombineHandler = exposeDrawHandler('draw.uncombine');
// emits 'drawModeChange'
export const drawModeChangeHandler = exposeDrawHandler('draw.modechange');
// emits 'drawActionable'
export const drawActionableHandler = exposeDrawHandler('draw.actionable');
// emits 'drawSelectionChange'
export const drawSelectionChangeHandler = exposeDrawHandler('draw.selectionchange');

export const locationsClickHandler = (emit: SetupContext['emit']) => ({
	type: 'click',
	layer: LayerTypes.Locations,
	handler: (ev: EventData) => {
		const queryResults = ev.target.queryRenderedFeatures(ev.point);
		queryResults && queryResults[0].properties?.id && emit('locationClick', queryResults[0].properties?.id);
	},
});

export const polygonClickHandler = (emit: SetupContext['emit']) => ({
	type: 'click',
	layer: LayerTypes.FieldPolygon,
	handler: (ev: EventData) => {
		const queryResults = ev.target.queryRenderedFeatures(ev.point);
		queryResults && queryResults[0].properties?.id && emit('polygonClick', queryResults[0].properties?.id);
	},
});

export const markerClickHandler = (emit: SetupContext['emit']) => ({
	type: 'click',
	layer: LayerTypes.Markers,
	handler: (ev: EventData) => {
		const queryResults = ev.target.queryRenderedFeatures(ev.point);
		queryResults && queryResults[0].properties?.id && emit('markerClick', queryResults[0].properties?.id);
	},
});

export function createActiveLocationLayer(activeLocation: Feature): Layer {
	return {
		id: LayerTypes.ActiveLocation,
		type: 'symbol',
		source: {
			type: 'geojson',
			data: activeLocation || [],
			cluster: false,
		},
		layout: {
			'icon-image': 'circle',
			'icon-size': 1,
			'text-offset': [0, -0.5],
			'text-allow-overlap': true,
			'icon-allow-overlap': true,
		},
		paint: {
			'icon-color': '#ff00ff',
			'icon-opacity': 0.5,
		},
	};
}

export function createLocationCollectionLayer(locationCollection: FeatureCollection): Layer {
	return {
		id: LayerTypes.Locations,
		type: 'symbol',
		source: {
			type: 'geojson',
			data: locationCollection || [],
			cluster: false,
		},
		layout: {
			'text-size': {
				stops: [
					[16, 0],
					[18, 24],
				],
			},
			'icon-image': [
				'match',
				['get', 'markerType'],
				'LOCATION_INCOMPLETE',
				'circle-stroked-11',
				'LOCATION_COMPLETE',
				'triangle-15',
				'circle-15',
			],
			'icon-size': 1,
			'text-field': ['get', 'label'],
			'text-offset': [0, -0.75],
			'text-allow-overlap': true,
			'icon-allow-overlap': true,
		},
		paint: {
			'icon-color': '#ff00ff',
			'icon-halo-blur': 10,
			'icon-halo-color': 'rgba(0, 0, 0, 255)',
			'icon-halo-width': 10,
			'text-halo-blur': 1,
			'text-halo-color': 'rgba(0, 0, 0, 255)',
			'text-halo-width': 1,
			'text-color': '#ffffff',
		},
	};
}

export function createMarkersLayers(markers: Feature<Point>[], active: boolean): Layer {
	return {
		id: active ? LayerTypes.ActiveMarkers : LayerTypes.Markers,
		type: 'symbol',
		source: {
			type: 'geojson',
			data: {
				type: 'FeatureCollection',
				features: markers,
			},
		},
		layout: {
			'icon-image': active ? 'activePin' : 'pin',
			'icon-anchor': 'bottom',
			'icon-size': 0.25,
			'icon-allow-overlap': true,
		},
	};
}

export function createAreaLayers({
	id,
	areas,
	paint,
	layout = {},
}: {
	id: LayerTypes;
	areas: Feature<Polygon>[];
	paint?: AnyPaint;
	layout?: AnyLayout;
}): Layer {
	return {
		id,
		type: id === LayerTypes.FieldPolygon || id === LayerTypes.ActiveFieldPolygon ? 'fill' : 'line',
		source: {
			type: 'geojson',
			data: {
				type: 'FeatureCollection',
				features: areas,
			},
		},
		paint,
		layout,
	};
}

export function createLayers({
	areas,
	activeAreas,
	locations,
	activeLocation,
	markers,
	activeMarkers,
	polygon,
}: {
	areas?: Feature<Polygon>[] | null;
	activeAreas?: Feature<Polygon>[] | null;
	locations?: FeatureCollection | null;
	activeLocation?: Feature | null;
	markers?: Feature<Point>[] | null;
	activeMarkers?: Feature<Point>[] | null;
	polygon?: boolean;
}): Layer[] {
	const layers: Layer[] = [];

	if (areas) {
		layers.push(
			createAreaLayers({
				id: LayerTypes.FieldBoundary,
				areas,
				paint: { 'line-color': '#3bb2d0', 'line-width': polygon ? 1 : 2 },
			})
		);
		if (polygon) {
			layers.push(
				createAreaLayers({
					id: LayerTypes.FieldPolygon,
					areas,
					paint: { 'fill-color': '#3bb2d0', 'fill-opacity': 0.25 },
					layout: { 'fill-sort-key': 0 },
				})
			);
		}
	}

	if (activeAreas) {
		layers.push(
			createAreaLayers({
				id: LayerTypes.ActiveFieldBoundary,
				areas: activeAreas,
				paint: { 'line-color': '#6be2f0', 'line-width': polygon ? 1 : 2 },
			})
		);
		if (polygon) {
			layers.push(
				createAreaLayers({
					id: LayerTypes.ActiveFieldPolygon,
					areas: activeAreas,
					paint: { 'fill-color': '#1976d2', 'fill-opacity': 0.25 },
					layout: { 'fill-sort-key': 1 },
				})
			);
		}
	}

	if (locations) {
		layers.push(createLocationCollectionLayer(locations));
	}

	if (activeLocation) {
		layers.push(createActiveLocationLayer(activeLocation));
	}

	if (markers) {
		layers.push(createMarkersLayers(markers, false));
	}

	if (activeMarkers) {
		layers.push(createMarkersLayers(activeMarkers, true));
	}

	return layers;
}
