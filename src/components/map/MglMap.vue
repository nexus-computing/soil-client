<template>
	<div ref="componentRootElement" :id="mapId" class="map"></div>
</template>

<script lang="ts">
import {
	computed,
	defineComponent,
	onMounted,
	onUnmounted,
	Ref,
	ref,
	SetupContext,
	toRefs,
	watch,
	watchEffect,
} from '@vue/composition-api';
import { Control, EventData, GeoJSONSource, Layer, Map as MapboxMap } from 'mapbox-gl';
import {
	addControls,
	addEventHandlers,
	addLayers,
	ControlTypes,
	createControls,
	createMap,
	loadIcons,
	MapEventHandler,
} from '@/components/map/mglUtils';
import config from '@/configuration';
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css';
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';
import 'mapbox-gl/dist/mapbox-gl.css';
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css';
import 'mapbox-gl/dist/mapbox-gl.css';

const updateLayers = (map: MapboxMap) => (layers: Layer[], prevLayers: Layer[]) => {
	const layerIdsToRemove = prevLayers
		.map((prevLayer) => prevLayer.id)
		.filter((prevLayerId) => !layers.some((layer) => layer.id === prevLayerId));

	layerIdsToRemove.forEach((id) => {
		map.removeLayer(id);
		map.removeSource(id);
	});

	layers &&
		layers.forEach((layer: Layer) => {
			if (map.getLayer(layer.id)) {
				// eslint-disable-next-line @typescript-eslint/no-explicit-any
				const source = layer?.source as GeoJSONSource & { data: any };
				source && (map.getSource(layer.id) as any).setData(source.data);
			} else {
				map.addLayer(layer);
			}
		});
};

const disableMapPanWhenTouchingAnotherElement = (map: Ref, componentRootElement: Ref<HTMLElement | null>) => (
	ev: TouchEvent
) => {
	if (!componentRootElement.value?.contains(ev.target as Element)) {
		map.value?.dragPan.disable();
	} else {
		map.value?.dragPan.enable();
	}
};
const enableMapPan = (map: Ref) => () => {
	map.value?.dragPan.enable();
};

export default defineComponent({
	props: {
		id: {
			type: String,
			required: false,
		},
		layers: {
			type: Array as () => Layer[],
			default: () => [],
		},
		controls: {
			type: Array as () => ControlTypes[],
			default: () => [],
		},
		eventHandlers: {
			type: Array as () => Array<(emit: SetupContext['emit']) => MapEventHandler>,
			default: () => [],
		},
		bounds: {
			default: null,
			type: Array,
		},
		fitBoundsOptions: {
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			type: Object as () => undefined | null | { [key: string]: any },
		},
		zoom: {
			type: Number,
			default: 9,
		},
		center: {
			type: Array,
		},
		maxBounds: {
			type: Array, // LngLatBoundsLike
			default: () => null,
		},
		tileset: {
			type: String,
			default: config.mapbox.tileset,
		},
	},
	emits: ['trackUserLocationStart', 'trackUserLocationEnd', 'locationClick', 'polygonClick', 'markerClick', 'loaded'],
	setup(props, { emit, root }) {
		const { id, center, bounds, fitBoundsOptions, maxBounds, zoom, tileset, layers, controls, eventHandlers } = props;
		const { layers: layersRef } = toRefs(props);
		const componentRootElement = ref<HTMLElement | null>(null);
		const map = ref<MapboxMap>();
		const mapId = id ?? `map-${Math.floor(Math.random() * 1e6)}`;
		const mapControls: Ref<{ [key: string]: Control }> = ref({});
		const pointToFly = computed<[number, number] | null>(() => root.$store.getters['map/getPointToFlyByMapId'](mapId));

		// create event handlers specific to this map so removeEventListener doesn't remove listeners for another map
		const disablePan = disableMapPanWhenTouchingAnotherElement(map, componentRootElement);
		const enablePan = enableMapPan(map);

		watchEffect((cleanup) => {
			// prevent issue on iOS Safari where scrolling/dragging an element near the the map can simultaneously pan the map
			window.addEventListener('touchstart', disablePan);
			window.addEventListener('touchcancel', enablePan);
			window.addEventListener('touchend', enablePan);

			cleanup(() => {
				window.removeEventListener('touchstart', disablePan);
				window.removeEventListener('touchcancel', enablePan);
				window.removeEventListener('touchend', enablePan);
			});
		});

		onMounted(() => {
			root.$store.dispatch('map/clearPointToFly', mapId);

			map.value = createMap({
				container: mapId,
				accessToken: process.env.VUE_APP_MAPBOX_TOKEN,
				center,
				bounds,
				fitBoundsOptions,
				maxBounds,
				zoom,
				tileset,
				dragRotate: false,
				pitchWithRotate: false,
				touchPitch: false,
			});
			map.value.touchZoomRotate.disableRotation();

			map.value.on('load', () => {
				if (!map.value) {
					return;
				}

				loadIcons(map.value);
				if (layers.length > 0) {
					addLayers(map.value, layers);
				}
				if (controls) {
					mapControls.value = createControls(controls);
					addControls(map.value, mapControls.value);
					if (ControlTypes.Geolocate in mapControls.value) {
						mapControls.value[ControlTypes.Geolocate].on('trackuserlocationstart', () => {
							emit('trackUserLocationStart');
						});
						mapControls.value[ControlTypes.Geolocate].on('trackuserlocationend', (ev: EventData) => {
							if (ev.target._watchState === 'OFF') {
								// 'trackuserlocationend' gets emitted by MapboxGL when user pans map,
								// and in other situations. Let's only fire our event when user turns off GPS.
								emit('trackUserLocationEnd');
							}
						});
					}
				}
				watch(layersRef, updateLayers(map.value));
				emit('loaded');
			});

			if (eventHandlers.length > 0) {
				addEventHandlers(emit, map.value, eventHandlers);
			}
		});

		// Pan to the active marker
		watchEffect(() => {
			if (map.value && pointToFly.value) {
				map.value.flyTo({
					center: pointToFly.value,
					zoom: 15,
				});
				root.$store.dispatch('map/clearPointToFly', mapId);
			}
		});

		onUnmounted(() => {
			root.$store.dispatch('map/clearPointToFly', mapId);
		});

		return {
			componentRootElement,
			map,
			mapId,
			mapControls,
		};
	},
});
</script>

<style scoped>
.map {
	height: 60vh;
}

.mapboxgl-popup-content {
	padding: 10px !important;
}
</style>
