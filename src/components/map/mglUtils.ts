import { SetupContext } from '@vue/composition-api';
import MapboxDraw from '@mapbox/mapbox-gl-draw';
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import MapboxGL, { Control, EventData, GeolocateControl, Layer, Map as MapboxMap, MapLayerEventType } from 'mapbox-gl';
import circleIcon from '@/components/map/assets/circle-fill.png';
import activePinIcon from '@/components/map/assets/mb-marker-active-128.png';
import pinIcon from '@/components/map/assets/mb-marker-default-128.png';
import { notEmpty } from '@/utils/typeHelpers';
import drawStyles from './drawStyles';

export enum ControlTypes {
	Geolocate = 'GEOLOCATE',
	Draw = 'DRAW',
	Geocoder = 'GEOCODER',
}

export function createMap({
	container = 'map',
	accessToken = '',
	center = [-84.55068956887425, 39.08187355127035],
	zoom = 12,
	bounds,
	fitBoundsOptions,
	maxBounds,
	tileset,
}: // eslint-disable-next-line @typescript-eslint/no-explicit-any
{ [key: string]: any } = {}) {
	MapboxGL.accessToken = accessToken;
	const map = new MapboxMap({
		container,
		style: `mapbox://styles/mapbox/${tileset}`, // stylesheet location
		center,
		zoom,
		bounds,
		fitBoundsOptions,
		maxBounds,
		refreshExpiredTiles: false,
	});
	return map;
}

export function createControls(controls: ControlTypes[]): { [key: string]: Control } {
	return controls
		.map((control) => {
			switch (control) {
				case ControlTypes.Geolocate:
					return {
						[ControlTypes.Geolocate]: new GeolocateControl({
							positionOptions: {
								enableHighAccuracy: true,
							},
							trackUserLocation: true,
							showAccuracyCircle: true,
							showUserLocation: true,
						}),
					};
				case ControlTypes.Draw:
					return {
						[ControlTypes.Draw]: new MapboxDraw({
							styles: drawStyles,
							userProperties: true,
							displayControlsDefault: false,
						}),
					};
				case ControlTypes.Geocoder:
					return {
						[ControlTypes.Geocoder]: new MapboxGeocoder({
							marker: false,
							accessToken: process.env.VUE_APP_MAPBOX_TOKEN ?? '',
							collapsed: true,
						}),
					};
				default:
					return null;
			}
		})
		.filter(notEmpty)
		.reduce((r, x) => ({ ...r, ...x }), {});
}

export function addControls(map: MapboxMap, controls: { [key: string]: Control }): void {
	Object.values(controls).forEach((control) => map.addControl(control));
}

export function addLayers(map: MapboxMap, layers: Layer[] = []) {
	layers.forEach((layer) => map.addLayer(layer));
}

export interface MapEventHandler {
	type: keyof MapLayerEventType;
	layer: string;
	handler: (ev: EventData) => void;
}

export function addEventHandlers(
	emit: SetupContext['emit'],
	map: MapboxMap,
	handlers: Array<(emit: SetupContext['emit']) => MapEventHandler>
) {
	handlers
		.map((handler) => handler(emit))
		.forEach(({ type, layer, handler }) => {
			if (layer) {
				map.on(type, layer, handler);
			} else {
				map.on(type, handler);
			}
		});
}

export function loadIcons(map: MapboxMap) {
	const emptyImage = { width: 0, height: 0, data: new Uint8Array() };

	[
		['pin', pinIcon],
		['activePin', activePinIcon],
		['circle', circleIcon, { sdf: true }],
	].forEach(([key, source, options]) => {
		// Sometimes, you got warning `image is not loaded` in the browser console,
		// to prevent this, added empty image first and replace with real one once loaded.
		map.addImage(key, emptyImage);

		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		map.loadImage(source, (error: any, image: any) => {
			if (error) throw error;
			map.removeImage(key);
			map.addImage(key, image, options);
		});
	});
}
