/*
	There is a bug in mapbox where the `_updateCamera` method of the GeolocateControl does not check the map's `maxBounds` before panning the camera.
	This can result in the camera jumping around and the map tiles flickering black. Because we limit the map's bounds in this application as part
	of supporting offline maps, the camera following your location isn't especially useful. Replacing the `_updateCamera` method with a noop prevents
	the flickering.
*/

import { GeolocateControl } from 'mapbox-gl';
// eslint-disable-next-line @typescript-eslint/no-explicit-any
(GeolocateControl.prototype as any)._updateCamera = () => {
	/* do nothing */
};
