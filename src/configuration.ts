export default {
	map: {
		padBoundsPercentage: 0.2, // percentage of field dimension (width or height) to add on each side of bounds.
		offline: {
			// number of extra zoom levels to fetch tiles for offline
			extraZoomLevels: 1,
		},
	},
	mapbox: {
		tileset: 'satellite-v9', // default tileset, and tileset used for offline maps
	},
};
