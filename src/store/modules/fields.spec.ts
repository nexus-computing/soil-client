/* eslint-disable @typescript-eslint/no-explicit-any */
import createTestStore from '../../../tests/createTestStore';
import soilApiService from '../../services/soilApiService';
import createFieldsModule, { actions, getters, useCreateField, useDeleteField, useEditField } from './fields';
import { createAsyncState, createMockFieldState } from './test-utils';

jest.mock('../../services/soilApiService');

describe('fields module', () => {
	describe('actions', () => {
		let commitSpy: jest.Mock;

		beforeEach(() => {
			commitSpy = jest.fn();
		});

		describe('requestDelete', () => {
			it("commits 'requestDelete' mutation before making request", async () => {
				await (actions.requestDelete as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestDelete');
			});

			it("commits 'requestDeleteSuccess' mutation on success", async () => {
				(soilApiService.deleteField as jest.Mock).mockResolvedValue('ok');

				await (actions.requestDelete as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestDeleteSuccess');
			});

			it("commits 'requestDeleteError' mutation on error", async () => {
				const error = new Error('error');
				(soilApiService.deleteField as jest.Mock).mockRejectedValueOnce(error);

				await (actions.requestDelete as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestDeleteError', error);
			});
		});

		describe('acknowledgeDeleteError', () => {
			it("commits 'resetDeleteRequest' mutation", async () => {
				await (actions.acknowledgeDeleteError as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('resetDeleteRequest');
			});
		});

		describe('requestEdit', () => {
			it("commits 'requestEdit' mutation before making request", async () => {
				await (actions.requestEdit as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestEdit');
			});

			it("commits 'requestEditSuccess' mutation on success", async () => {
				(soilApiService.editField as jest.Mock).mockResolvedValue('ok');

				await (actions.requestEdit as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestEditSuccess');
			});

			it("commits 'requestEditError' mutation on error", async () => {
				const error = new Error('error');
				(soilApiService.editField as jest.Mock).mockRejectedValueOnce(error);

				await (actions.requestEdit as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestEditError', error);
			});
		});

		describe('acknowledgeEditError', () => {
			it("commits 'resetEditRequest' mutation", async () => {
				await (actions.acknowledgeEditError as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('resetEditRequest');
			});
		});

		describe('requestCreate', () => {
			it("commits 'requestCreate' mutation before making request", async () => {
				await (actions.requestCreate as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestCreate');
			});

			it("commits 'requestCreateSuccess' mutation on success", async () => {
				(soilApiService.createField as jest.Mock).mockResolvedValue('ok');

				await (actions.requestCreate as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestCreateSuccess');
			});

			it("commits 'requestCreateError' mutation on error", async () => {
				const error = new Error('error');
				(soilApiService.createField as jest.Mock).mockRejectedValueOnce(error);

				await (actions.requestCreate as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestCreateError', error);
			});
		});

		describe('acknowledgeCreateError', () => {
			it("commits 'resetCreateRequest' mutation", async () => {
				await (actions.acknowledgeCreateError as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('resetCreateRequest');
			});
		});
	});

	describe('mutations', () => {
		let fieldsModule: any;

		beforeEach(() => {
			fieldsModule = createFieldsModule();
		});

		describe('requestDelete', () => {
			it('sets isLoading to true, isLoaded to false, error to null', () => {
				fieldsModule.state = createMockFieldState({
					delete: createAsyncState({
						isLoading: false,
						isLoaded: true,
						error: 'error',
					}),
				});

				fieldsModule.mutations.requestDelete(fieldsModule.state);

				expect(fieldsModule.state.delete).toHaveProperty('isLoading', true);
				expect(fieldsModule.state.delete).toHaveProperty('isLoaded', false);
				expect(fieldsModule.state.delete).toHaveProperty('error', null);
			});
		});

		describe('requestDeleteSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null', () => {
				fieldsModule.state = createMockFieldState({
					delete: createAsyncState({
						isLoading: true,
						isLoaded: false,
						error: 'error',
					}),
				});

				fieldsModule.mutations.requestDeleteSuccess(fieldsModule.state);

				expect(fieldsModule.state.delete).toHaveProperty('isLoading', false);
				expect(fieldsModule.state.delete).toHaveProperty('isLoaded', true);
				expect(fieldsModule.state.delete).toHaveProperty('error', null);
			});
		});

		describe('requestDeleteError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error ', () => {
				fieldsModule.state = createMockFieldState({
					delete: createAsyncState({
						isLoading: true,
						isLoaded: true,
						error: null,
					}),
				});

				fieldsModule.mutations.requestDeleteError(fieldsModule.state, 'error');

				expect(fieldsModule.state.delete).toHaveProperty('isLoading', false);
				expect(fieldsModule.state.delete).toHaveProperty('isLoaded', false);
				expect(fieldsModule.state.delete).toHaveProperty('error', 'error');
			});
		});

		describe('resetDeleteRequest', () => {
			it('sets isLoading to false and error to null', () => {
				fieldsModule.state = createMockFieldState({
					delete: createAsyncState({
						isLoading: true,
						error: 'error',
					}),
				});

				fieldsModule.mutations.resetDeleteRequest(fieldsModule.state);

				expect(fieldsModule.state.delete).toHaveProperty('isLoading', false);
				expect(fieldsModule.state.delete).toHaveProperty('error', null);
			});
		});

		describe('requestEdit', () => {
			it('sets isLoading to true, isLoaded to false, error to null', () => {
				fieldsModule.state = createMockFieldState({
					edit: createAsyncState({
						isLoading: false,
						isLoaded: true,
						error: 'error',
					}),
				});

				fieldsModule.mutations.requestEdit(fieldsModule.state);

				expect(fieldsModule.state.edit).toHaveProperty('isLoading', true);
				expect(fieldsModule.state.edit).toHaveProperty('isLoaded', false);
				expect(fieldsModule.state.edit).toHaveProperty('error', null);
			});
		});

		describe('requestEditSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null', () => {
				fieldsModule.state = createMockFieldState({
					edit: createAsyncState({
						isLoading: true,
						isLoaded: false,
						error: 'error',
					}),
				});

				fieldsModule.mutations.requestEditSuccess(fieldsModule.state);

				expect(fieldsModule.state.edit).toHaveProperty('isLoading', false);
				expect(fieldsModule.state.edit).toHaveProperty('isLoaded', true);
				expect(fieldsModule.state.edit).toHaveProperty('error', null);
			});
		});

		describe('requestEditError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error ', () => {
				fieldsModule.state = createMockFieldState({
					edit: createAsyncState({
						isLoading: true,
						isLoaded: true,
						error: null,
					}),
				});

				fieldsModule.mutations.requestEditError(fieldsModule.state, 'error');

				expect(fieldsModule.state.edit).toHaveProperty('isLoading', false);
				expect(fieldsModule.state.edit).toHaveProperty('isLoaded', false);
				expect(fieldsModule.state.edit).toHaveProperty('error', 'error');
			});
		});

		describe('resetEditRequest', () => {
			it('sets isLoading to false and error to null', () => {
				fieldsModule.state = createMockFieldState({
					edit: createAsyncState({
						isLoading: true,
						error: 'error',
					}),
				});

				fieldsModule.mutations.resetEditRequest(fieldsModule.state);

				expect(fieldsModule.state.edit).toHaveProperty('isLoading', false);
				expect(fieldsModule.state.edit).toHaveProperty('error', null);
			});
		});

		describe('requestCreate', () => {
			it('sets isLoading to true, isLoaded to false, error to null', () => {
				fieldsModule.state = createMockFieldState({
					create: createAsyncState({
						isLoading: false,
						isLoaded: true,
						error: 'error',
					}),
				});

				fieldsModule.mutations.requestCreate(fieldsModule.state);

				expect(fieldsModule.state.create).toHaveProperty('isLoading', true);
				expect(fieldsModule.state.create).toHaveProperty('isLoaded', false);
				expect(fieldsModule.state.create).toHaveProperty('error', null);
			});
		});

		describe('requestCreateSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null', () => {
				fieldsModule.state = createMockFieldState({
					create: createAsyncState({
						isLoading: true,
						isLoaded: false,
						error: 'error',
					}),
				});

				fieldsModule.mutations.requestCreateSuccess(fieldsModule.state);

				expect(fieldsModule.state.create).toHaveProperty('isLoading', false);
				expect(fieldsModule.state.create).toHaveProperty('isLoaded', true);
				expect(fieldsModule.state.create).toHaveProperty('error', null);
			});
		});

		describe('requestCreateError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error ', () => {
				fieldsModule.state = createMockFieldState({
					create: createAsyncState({
						isLoading: true,
						isLoaded: true,
						error: null,
					}),
				});

				fieldsModule.mutations.requestCreateError(fieldsModule.state, 'error');

				expect(fieldsModule.state.create).toHaveProperty('isLoading', false);
				expect(fieldsModule.state.create).toHaveProperty('isLoaded', false);
				expect(fieldsModule.state.create).toHaveProperty('error', 'error');
			});
		});

		describe('resetCreateRequest', () => {
			it('sets isLoading to false and error to null', () => {
				fieldsModule.state = createMockFieldState({
					create: createAsyncState({
						isLoading: true,
						error: 'error',
					}),
				});

				fieldsModule.mutations.resetCreateRequest(fieldsModule.state);

				expect(fieldsModule.state.create).toHaveProperty('isLoading', false);
				expect(fieldsModule.state.create).toHaveProperty('error', null);
			});
		});
	});

	describe('getters', () => {
		describe('getIsDeleteLoading', () => {
			it('returns isLoading', () => {
				const state = createMockFieldState({
					delete: createAsyncState({ isLoading: true }),
				});

				const actual = (getters as any).getIsDeleteLoading(state);

				expect(actual).toBe(true);
			});
		});

		describe('getIsDeleteLoaded', () => {
			it('returns isLoaded', () => {
				const state = createMockFieldState({
					delete: createAsyncState({ isLoaded: true }),
				});

				const actual = (getters as any).getIsDeleteLoaded(state);

				expect(actual).toBe(true);
			});
		});

		describe('getHasDeleteError', () => {
			it('returns true if error is not null', () => {
				const state = createMockFieldState({
					delete: createAsyncState({ error: 'error' }),
				});

				const actual = (getters as any).getHasDeleteError(state);

				expect(actual).toBe(true);
			});

			it('returns false if error is null', () => {
				const state = createMockFieldState({
					delete: createAsyncState({ error: null }),
				});

				const actual = (getters as any).getHasDeleteError(state);

				expect(actual).toBe(false);
			});
		});

		describe('getDeleteError', () => {
			it('returns error', () => {
				const state = createMockFieldState({
					delete: createAsyncState({ error: 'error' }),
				});

				const actual = (getters as any).getDeleteError(state);

				expect(actual).toEqual('error');
			});
		});

		describe('getIsEditLoading', () => {
			it('returns isLoading', () => {
				const state = createMockFieldState({
					edit: createAsyncState({ isLoading: true }),
				});

				const actual = (getters as any).getIsEditLoading(state);

				expect(actual).toBe(true);
			});
		});

		describe('getIsEditLoaded', () => {
			it('returns isLoaded', () => {
				const state = createMockFieldState({
					edit: createAsyncState({ isLoaded: true }),
				});

				const actual = (getters as any).getIsEditLoaded(state);

				expect(actual).toBe(true);
			});
		});

		describe('getHasEditError', () => {
			it('returns true if error is not null', () => {
				const state = createMockFieldState({
					edit: createAsyncState({ error: 'error' }),
				});

				const actual = (getters as any).getHasEditError(state);

				expect(actual).toBe(true);
			});

			it('returns false if error is null', () => {
				const state = createMockFieldState({
					edit: createAsyncState({ error: null }),
				});

				const actual = (getters as any).getHasEditError(state);

				expect(actual).toBe(false);
			});
		});

		describe('getEditError', () => {
			it('returns error', () => {
				const state = createMockFieldState({
					edit: createAsyncState({ error: 'error' }),
				});

				const actual = (getters as any).getEditError(state);

				expect(actual).toEqual('error');
			});
		});

		describe('getIsCreateLoading', () => {
			it('returns isLoading', () => {
				const state = createMockFieldState({
					create: createAsyncState({ isLoading: true }),
				});

				const actual = (getters as any).getIsCreateLoading(state);

				expect(actual).toBe(true);
			});
		});

		describe('getIsCreateLoaded', () => {
			it('returns isLoaded', () => {
				const state = createMockFieldState({
					create: createAsyncState({ isLoaded: true }),
				});

				const actual = (getters as any).getIsCreateLoaded(state);

				expect(actual).toBe(true);
			});
		});

		describe('getHasCreateError', () => {
			it('returns true if error is not null', () => {
				const state = createMockFieldState({
					create: createAsyncState({ error: 'error' }),
				});

				const actual = (getters as any).getHasCreateError(state);

				expect(actual).toBe(true);
			});

			it('returns false if error is null', () => {
				const state = createMockFieldState({
					create: createAsyncState({ error: null }),
				});

				const actual = (getters as any).getHasCreateError(state);

				expect(actual).toBe(false);
			});
		});

		describe('getCreateError', () => {
			it('returns error', () => {
				const state = createMockFieldState({
					create: createAsyncState({ error: 'error' }),
				});

				const actual = (getters as any).getCreateError(state);

				expect(actual).toEqual('error');
			});
		});
	});

	describe('hooks', () => {
		describe('useDeleteField', () => {
			it('returns computedRefs of fields state', () => {
				const store = createTestStore();
				const fields = createMockFieldState({
					delete: createAsyncState({
						isLoading: false,
						isLoaded: false,
						error: 'delete error',
					}),
				});
				store.replaceState({
					...store.state,
					fields,
				});

				const actual = useDeleteField(store);

				expect(actual.isLoading.value).toEqual(false);
				expect(actual.isLoaded.value).toEqual(false);
				expect(actual.hasError.value).toEqual(true);
				expect(actual.error.value).toEqual('delete error');
			});
		});

		describe('useEditField', () => {
			it('returns computedRefs of fields state', () => {
				const store = createTestStore();
				const fields = createMockFieldState({
					edit: createAsyncState({
						isLoading: false,
						isLoaded: false,
						error: 'edit error',
					}),
				});
				store.replaceState({
					...store.state,
					fields,
				});

				const actual = useEditField(store);

				expect(actual.isLoading.value).toEqual(false);
				expect(actual.isLoaded.value).toEqual(false);
				expect(actual.hasError.value).toEqual(true);
				expect(actual.error.value).toEqual('edit error');
			});
		});

		describe('useCreateField', () => {
			it('returns computedRefs of fields state', () => {
				const store = createTestStore();
				const fields = createMockFieldState({
					create: createAsyncState({
						isLoading: false,
						isLoaded: false,
						error: 'create error',
					}),
				});
				store.replaceState({
					...store.state,
					fields,
				});

				const actual = useCreateField(store);

				expect(actual.isLoading.value).toEqual(false);
				expect(actual.isLoaded.value).toEqual(false);
				expect(actual.hasError.value).toEqual(true);
				expect(actual.error.value).toEqual('create error');
			});
		});
	});
});
