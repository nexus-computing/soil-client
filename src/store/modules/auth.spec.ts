import createTestStore from '../../../tests/createTestStore';
import { AuthState, User } from './auth';

const createMockUser = ({
	authProviders = [],
	email = '',
	memberships = [],
	name = '',
	permissions = [],
	roles = [],
	token = '',
	_id = '',
}: Partial<User> = {}): User => ({
	authProviders,
	email,
	memberships,
	name,
	permissions,
	roles,
	token,
	_id,
});

const createMockAuthState = ({ status = '', user = createMockUser(), header = '' } = {}): AuthState => ({
	status,
	user,
	header,
});

describe('getters', () => {
	let store: any;
	beforeEach(() => {
		store = createTestStore();
	});

	describe('authStatus', () => {
		it('returns status', () => {
			store.replaceState({
				...store.state,
				auth: createMockAuthState({ status: 'success' }),
			});

			expect(store.getters['auth/authStatus']).toEqual('success');
		});
	});

	describe('isLoggedIn', () => {
		it('returns true if status is success', () => {
			store.replaceState({
				...store.state,
				auth: createMockAuthState({ status: 'success' }),
			});

			expect(store.getters['auth/isLoggedIn']).toEqual(true);
		});

		it('returns false if status is not success', () => {
			store.replaceState({
				...store.state,
				auth: createMockAuthState({ status: '' }),
			});

			expect(store.getters['auth/isLoggedIn']).toEqual(false);
		});
	});

	describe('isAdmin', () => {
		it('returns true if user has admin permission', () => {
			store.replaceState({
				...store.state,
				auth: createMockAuthState({
					user: createMockUser({ permissions: ['admin'] }),
				}),
			});

			expect(store.getters['auth/isAdmin']).toEqual(true);
		});

		it('returns false if user does not have admin permission', () => {
			store.replaceState({
				...store.state,
				auth: createMockAuthState({
					user: createMockUser({ permissions: [] }),
				}),
			});

			expect(store.getters['auth/isAdmin']).toEqual(false);
		});
	});

	describe('isSuperAdmin', () => {
		it('returns true if user has super-admin permission', () => {
			store.replaceState({
				...store.state,
				auth: createMockAuthState({
					user: createMockUser({ permissions: ['super-admin'] }),
				}),
			});

			expect(store.getters['auth/isSuperAdmin']).toEqual(true);
		});

		it('returns false if user does not have super-admin permission', () => {
			store.replaceState({
				...store.state,
				auth: createMockAuthState({
					user: createMockUser({ permissions: [] }),
				}),
			});

			expect(store.getters['auth/isSuperAdmin']).toEqual(false);
		});
	});

	describe('user', () => {
		it('returns user', () => {
			const mockUser = createMockUser();
			store.replaceState({
				...store.state,
				auth: createMockAuthState({ user: mockUser }),
			});

			expect(store.getters['auth/user']).toBe(mockUser);
		});
	});

	describe('getAuthorizationHeaderValue', () => {
		it('returns authorization header value', () => {
			store.replaceState({
				...store.state,
				auth: createMockAuthState({ header: 'email token' }),
			});

			expect(store.getters['auth/getAuthorizationHeaderValue']).toEqual('email token');
		});
	});

	describe('getRoles', () => {
		it('returns user roles', () => {
			const mockRoles = ['public', 'admin@/group/', 'admin@/group/subgroup/'];
			store.replaceState({
				...store.state,
				auth: createMockAuthState({
					user: createMockUser({ roles: mockRoles }),
				}),
			});

			expect(store.getters['auth/getRoles']).toBe(mockRoles);
		});
	});
});
