import { computed } from '@vue/composition-api';
import { ActionTree, GetterTree, Module, MutationTree, Store } from 'vuex';
import soilApiService from '@/services/soilApiService';
import { AsyncState, RootState } from '@/store/types';

function createInitialState() {
	return {
		isLoading: false,
		isLoaded: false,
		error: null,
	};
}

const actions: ActionTree<AsyncState, RootState> = {
	async requestDelete({ commit }, stratificationId: string): Promise<boolean> {
		commit('requestDelete');
		try {
			await soilApiService.deleteStratification(stratificationId);
			commit('requestDeleteSuccess');
			return true;
		} catch (error) {
			commit('requestDeleteError', error);
			return false;
		}
	},
	acknowledgeDeleteError({ commit }) {
		commit('resetDeleteRequest');
	},
};

const mutations: MutationTree<AsyncState> = {
	requestDelete(state) {
		state.isLoading = true;
		state.isLoaded = false;
		state.error = null;
	},
	requestDeleteSuccess(state) {
		state.isLoading = false;
		state.isLoaded = true;
		state.error = null;
	},
	requestDeleteError(state, error) {
		state.isLoading = false;
		state.isLoaded = false;
		state.error = error;
	},
	resetDeleteRequest(state) {
		state.isLoading = false;
		state.error = null;
	},
};

const getters: GetterTree<AsyncState, RootState> = {
	getIsLoading: (state) => state.isLoading,
	getIsLoaded: (state) => state.isLoaded,
	getHasError: (state) => Boolean(state.error),
	getError: (state) => state.error,
};

const createStratificationsModule = (): Module<AsyncState, RootState> => ({
	namespaced: true,
	actions,
	state: createInitialState(),
	mutations,
	getters,
});

const useStratifications = (store: Store<RootState>) => {
	const deleteStratification = (stratificationId: string): Promise<boolean> =>
		store.dispatch('stratifications/requestDelete', stratificationId);
	const clearError = () => store.dispatch('fields/acknowledgeDeleteError');
	const isLoading = computed(() => store.getters['stratifications/getIsLoading']);
	const isLoaded = computed(() => store.getters['stratifications/getIsLoaded']);
	const hasError = computed(() => store.getters['stratifications/getHasError']);
	const error = computed(() => store.getters['stratifications/getError']);

	return {
		deleteStratification,
		clearError,
		isLoading,
		isLoaded,
		hasError,
		error,
	};
};

export { actions, getters, mutations, useStratifications };

export default createStratificationsModule;
