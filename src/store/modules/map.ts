import { ActionTree, GetterTree, Module, MutationTree } from 'vuex';
import Vue from 'vue';
import { RootState } from '@/store/types';

interface PointToFlyPayload {
	mapId: string;
	coordinates: [number, number];
}

export interface MapState {
	pointToFly: { [key: string]: [number, number] };
}

function createInitialState(): MapState {
	return {
		pointToFly: {},
	};
}

const actions: ActionTree<MapState, RootState> = {
	setPointToFly({ commit }, payload: PointToFlyPayload) {
		commit('setPointToFly', payload);
	},
	clearPointToFly({ commit }, payload: string) {
		commit('clearPointToFly', payload);
	},
};

const mutations: MutationTree<MapState> = {
	setPointToFly(state, { mapId, coordinates }: PointToFlyPayload) {
		Vue.set(state.pointToFly, mapId, coordinates);
	},
	clearPointToFly(state, payload: string) {
		Vue.delete(state.pointToFly, payload);
	},
};

const getters: GetterTree<MapState, RootState> = {
	getPointToFlyByMapId: (state) => (mapId: string) => state.pointToFly?.[mapId],
};

const createMapModule = (): Module<MapState, RootState> => ({
	namespaced: true,
	actions,
	state: createInitialState(),
	mutations,
	getters,
});

export { actions, getters, mutations };

export default createMapModule;
