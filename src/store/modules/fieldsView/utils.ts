import { DateRange, Field, FieldRow, Group, StatusOptions } from '@/store/types';
import { prettyGoodCenterOf } from '@/utils/geo';
import { TableColumns } from './configuration';

export function fieldToFieldRow(field: Field, rootGetters: any): FieldRow {
	const groupName = rootGetters['groups/getGroupNameById'](field.meta.groupId) || 'no group';
	const status = rootGetters['resources/getFieldStatusById'](field.id);
	const area = rootGetters['resources/getAreaById'](field.areas[0]);

	let coordinates;
	// guards against errors caused by nonsensical areas with coordinates like [0, 0]
	try {
		coordinates = prettyGoodCenterOf(area)?.geometry?.coordinates;
	} catch (err) {
		console.warn('Invalid Area Coordinates:', err);
	}

	return {
		id: field.id,
		groupId: field.meta.groupId,
		name: field.name,
		producerName: field.producerName,
		groupName,
		status,
		lngLat: coordinates ? [coordinates[0], coordinates[1]] : null,
	};
}

export const matchQuery = (query: string | null) => (fieldRow: FieldRow) => {
	if (query === null) {
		return true;
	}
	const { name, producerName, groupName, status } = fieldRow;
	const lowerCaseQuery = query.toLowerCase();
	return (
		name.toLowerCase().includes(lowerCaseQuery) ||
		producerName.toLowerCase().includes(lowerCaseQuery) ||
		groupName.toLowerCase().includes(lowerCaseQuery) ||
		status.state.toLowerCase().includes(lowerCaseQuery)
	);
};

export const matchGroupFilter = (groupFilter: string[]) => (fieldRow: FieldRow) =>
	groupFilter.length === 0 ? true : groupFilter.includes(fieldRow.groupId);

// start and end are expected to be dates, not datetimes. They should be initialized by passing a string of the format 'YYYY-MM-DD' into new Date().
export const matchRecentActivityFilter = (recentActivityFilter: DateRange) => (fieldRow: FieldRow) => {
	const {
		status: { timestamp },
	} = fieldRow;
	const { start, end } = recentActivityFilter;

	if (start === null && end === null) {
		return true;
	}

	const startOffset = start?.getTimezoneOffset() || 0;
	const startTime = start?.getTime() || 0;
	const startInUTC = new Date(startTime + startOffset * 60 * 1000);

	const endOffset = end?.getTimezoneOffset() || 0;
	const endCopy = end ? new Date(end) : new Date();
	const endTime = endCopy.setDate(endCopy.getDate() + 1) || 0;
	const endInUTC = new Date(endTime + endOffset * 60 * 1000);

	const lastActivity = new Date(timestamp);

	if (start !== null && end !== null) {
		return lastActivity >= startInUTC && lastActivity <= endInUTC;
	}
	if (start !== null) {
		return lastActivity >= startInUTC;
	}
	if (end !== null) {
		return lastActivity <= endInUTC;
	}
};

export const matchStatusFilter = (statusFilter: StatusOptions) => (fieldRow: FieldRow) =>
	statusFilter === null ? true : fieldRow.status.state === statusFilter;

/**
 * Get a date range from the query params
 */
export const getDateRangeFromParams = (params: string): DateRange => {
	const source = params || '';
	const millisecondsPerDay = 24 * 60 * 60 * 1000;

	if (source.includes('-')) {
		const [startYear, startMonth, startDay, endYear, endMonth, endDay] = source
			.split('-')
			.map((dateComponent) => parseInt(dateComponent, 10));

		const start =
			!isNaN(startYear) && !isNaN(startMonth) && !isNaN(startDay)
				? new Date(new Date(startYear, startMonth - 1, startDay).setHours(0, 0, 0, 0))
				: null;
		const end =
			!isNaN(endYear) && !isNaN(endMonth) && !isNaN(endDay)
				? new Date(new Date(endYear, endMonth - 1, endDay).setHours(0, 0, 0, 0))
				: null;
		return { start, end };
	}

	const period = parseInt(source, 10);
	if (!isNaN(period)) {
		const beginningOfDay = new Date(new Date().setHours(0, 0, 0, 0));
		const endOfDay = new Date(new Date().setHours(0, 0, 0, 0));
		const start = new Date(beginningOfDay.getTime() - period * millisecondsPerDay);
		const end = new Date(endOfDay);
		return { start, end };
	}
	return { start: null, end: null };
};

/**
 * Check the group
 */
export const addGroupFilter = (group: Group, groupFilter: string[], groups: Group[]): string[] => {
	const newGroupFilter = [
		...groupFilter,
		...groups.filter(({ path }) => path.startsWith(group.path)).map(({ id }) => id),
	];

	return newGroupFilter;
};

/**
 * Uncheck the group
 */
export const removeGroupFilter = (group: Group, groupFilter: string[]): string[] =>
	groupFilter.filter((groupId) => groupId !== group.id);

const statusStateSortOrder = {
	posted: 0,
	stratified: 1,
	sampled: 2,
};

/**
 * Include the IDs of parents from the groupIds
 */
export const getGroupIdsWithParent = (groupIds: string[], groups: Group[]): string[] => {
	const groupPaths = groups.filter((group) => groupIds.includes(group.id)).map((group) => group.path);
	const parentIds = groupPaths.flatMap((groupPath) =>
		groups.filter((group) => groupPath.startsWith(group.path)).map((group) => group.id)
	);
	return Array.from(new Set([...groupIds, ...parentIds]));
};

export const sortBys = {
	[TableColumns.NAME]: (a: FieldRow, b: FieldRow) => a.name.localeCompare(b.name),
	[TableColumns.PRODUCER_NAME]: (a: FieldRow, b: FieldRow) => a.producerName.localeCompare(b.producerName),
	[TableColumns.GROUP_NAME]: (a: FieldRow, b: FieldRow) => a.groupName.localeCompare(b.groupName),
	[TableColumns.STATUS]: (a: FieldRow, b: FieldRow) =>
		statusStateSortOrder[a.status.state] - statusStateSortOrder[b.status.state] ||
		a.status.timestamp.localeCompare(b.status.timestamp),
};
