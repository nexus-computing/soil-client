import { computed } from '@vue/composition-api';
import { ActionTree, GetterTree, Module, MutationTree, Store } from 'vuex';
import Vue from 'vue';
import { Field, FieldRow, Group, RootState, StatusOptions } from '@/store/types';
import {
	addGroupFilter,
	fieldToFieldRow,
	getDateRangeFromParams,
	getGroupIdsWithParent,
	matchGroupFilter,
	matchQuery,
	matchRecentActivityFilter,
	matchStatusFilter,
	removeGroupFilter,
	sortBys,
} from './utils';

type SortByOptions = 'name' | 'producerName' | 'groupName' | 'status' | null;
export type SortDirOptions = 'ASC' | 'DESC' | null;

export interface FieldsViewState {
	query: string | null;
	sortBy: SortByOptions;
	sortDir: SortDirOptions;
	groupFilter: string[];
	openedGroupIds: string[];
	recentActivityFilter: string | null;
	statusFilter: StatusOptions;
	activeFieldId: string | null;
}

function createInitialState(): FieldsViewState {
	return {
		query: null,
		sortBy: null,
		sortDir: null,
		groupFilter: [],
		openedGroupIds: [],
		recentActivityFilter: null,
		statusFilter: null,
		activeFieldId: null,
	};
}

const actions: ActionTree<FieldsViewState, RootState> = {
	setQuery({ commit }, query: string): void {
		commit('setQuery', query);
	},
	setSortBy({ commit }, sortBy: SortByOptions): void {
		commit('setSortBy', sortBy);
	},
	setSortDir({ commit }, sortDir: SortDirOptions): void {
		commit('setSortDir', sortDir);
	},
	changeSort({ commit }, { sortBy, sortDir }: { sortBy: SortByOptions; sortDir: SortDirOptions }): void {
		commit('setSortBy', sortBy);
		commit('setSortDir', sortDir);
	},
	setGroupFilter({ commit }, groupFilter: string[]): void {
		commit('setGroupFilter', groupFilter);
	},
	clearGroupFilter({ commit }): void {
		commit('clearGroupFilter');
	},
	setOpenedGroupIds({ commit }, payload: string[]) {
		commit('setOpenedGroupIds', payload);
	},
	setRecentActivityFilter({ commit }, payload: string | null): void {
		commit('setRecentActivityFilter', payload);
	},
	setStatusFilter({ commit }, statusFilter: StatusOptions): void {
		commit('setStatusFilter', statusFilter);
	},
	setAllFilters({ commit }, filters: Partial<FieldsViewState>): void {
		commit('setAllFilters', filters);
	},
	clearAllFilters({ commit }): void {
		commit('clearAllFilters');
	},
	setActiveFieldId({ commit }, payload: string) {
		commit('setActiveFieldId', payload);
	},
	clearActiveFieldId({ commit }) {
		commit('clearActiveFieldId');
	},
};

const mutations: MutationTree<FieldsViewState> = {
	setQuery(state, query: string): void {
		state.query = query;
	},
	setSortBy(state, sortBy: SortByOptions): void {
		state.sortBy = sortBy;
	},
	setSortDir(state, sortDir: SortDirOptions): void {
		state.sortDir = sortDir;
	},
	setGroupFilter(state, groupFilter: string[]): void {
		Vue.set(state, 'groupFilter', groupFilter);
	},
	clearGroupFilter(state): void {
		Vue.set(state, 'groupFilter', []);
	},
	setOpenedGroupIds(state, payload: string[]) {
		Vue.set(state, 'openedGroupIds', payload);
	},
	setRecentActivityFilter(state, payload: string | null): void {
		Vue.set(state, 'recentActivityFilter', payload);
	},
	setStatusFilter(state, statusFilter: StatusOptions): void {
		state.statusFilter = statusFilter;
	},
	setActiveFieldId(state, payload: string) {
		state.activeFieldId = payload;
	},
	clearActiveFieldId(state) {
		state.activeFieldId = null;
	},
	setAllFilters(state, filters: Partial<FieldsViewState>): void {
		Object.assign(state, { ...state, ...filters });
	},
	clearAllFilters(state): void {
		Object.assign(state, { ...createInitialState(), activeFieldId: state.activeFieldId });
	},
};

const getters: GetterTree<FieldsViewState, RootState> = {
	getQuery: (state) => state.query,
	getSortBy: (state) => state.sortBy,
	getSortDir: (state) => state.sortDir,
	getGroupFilter: (state) => state.groupFilter,
	getOpenedGroupIds: (state) => state.openedGroupIds,
	getStatusFilter: (state) => state.statusFilter,
	getActiveFieldId: (state) => state.activeFieldId,
	getRecentActivityFilter: (state) => state.recentActivityFilter,
	getRecentActivityFilterDateRange: (state, getters) => {
		const recentActivityFilter = getters.getRecentActivityFilter;
		return getDateRangeFromParams(recentActivityFilter);
	},
	getFieldRows: (state, getters, rootState, rootGetters) => {
		const fields: Field[] = rootGetters['resources/getFields'];
		const query = getters.getQuery;
		const sortBy = getters.getSortBy;
		const sortDir = getters.getSortDir;
		const groupFilter = getters.getGroupFilter;
		const statusFilter = getters.getStatusFilter;
		const { start, end } = getters.getRecentActivityFilterDateRange;

		const filteredFieldRows = fields
			.map((field) => fieldToFieldRow(field, rootGetters))
			.filter(matchGroupFilter(groupFilter))
			.filter(matchStatusFilter(statusFilter))
			.filter(matchQuery(query))
			.filter(
				matchRecentActivityFilter({
					start: start ? new Date(start.toISOString().slice(0, 10)) : null,
					end: end ? new Date(end.toISOString().slice(0, 10)) : null,
				})
			);

		if (sortBy && sortDir) {
			const sortedFieldRows = filteredFieldRows.sort(sortBys[sortBy]);
			return sortDir === 'ASC' ? sortedFieldRows : [...sortedFieldRows].reverse();
		} else {
			return filteredFieldRows;
		}
	},
	getFieldRowsLength: (state, getters) => getters.getFieldRows.length,
	getActiveFieldRow: (state, getters) => {
		const activeFieldId = getters.getActiveFieldId;
		return getters.getFieldRows.find((item: FieldRow) => item.id === activeFieldId);
	},
	getActiveFieldRowVisible: (state, getters) => {
		const activeFieldId = getters.getActiveFieldId;
		const activeFieldRow = getters.getActiveFieldRow;
		return Boolean(activeFieldId && activeFieldRow);
	},
};

const createFieldsViewModule = (): Module<FieldsViewState, RootState> => ({
	namespaced: true,
	state: createInitialState(),
	actions,
	mutations,
	getters,
});

const useFieldsView = (store: Store<RootState>) => {
	const groupFilter = computed<string[]>(() => store.getters['fieldsView/getGroupFilter']);
	const groups = computed<Group[]>(() => store.getters['groups/getGroups']);
	const openedGroupIds = computed<string[]>(() => store.getters['fieldsView/getOpenedGroupIds']);

	const setGroupFilter = (id: string) => {
		const group = store.getters['groups/getById'](id);
		if (group) {
			const isAdding = !groupFilter.value.includes(id);
			const newGroupFilter = isAdding
				? addGroupFilter(group, groupFilter.value, groups.value)
				: removeGroupFilter(group, groupFilter.value);

			// set new group filter
			store.dispatch('fieldsView/setGroupFilter', newGroupFilter);
			// expand the checked node
			store.dispatch('fieldsView/setOpenedGroupIds', [...openedGroupIds.value, ...newGroupFilter]);
		}
	};

	const setFiltersFromUrl = () => {
		const { searchParams } = new URL(window.location.href);

		// Restore URL from the filters if no params are there
		if (Array.from(searchParams).length === 0) {
			store.dispatch('fieldsView/setGroupFilter', groupFilter.value);
			return;
		}

		// Set filters from the URL if any params are set
		const [query, sortBy, sortDir, statusFilter, recentActivityFilter] = [
			'query',
			'sortBy',
			'sortDir',
			'statusFilter',
			'recentActivityFilter',
		].map((param) => searchParams.get(param));

		const newGroupFilter = searchParams.get('groupFilter')?.split(',') ?? [];

		store.dispatch('fieldsView/setAllFilters', {
			query,
			sortBy,
			sortDir,
			statusFilter,
			groupFilter: newGroupFilter,
			recentActivityFilter,
		});
	};

	// This will reset the open state of the group tree to be expanded all checked nodes
	const resetOpenedGroupIds = () => {
		const groupIdsFromFilter = getGroupIdsWithParent(groupFilter.value, groups.value);
		store.dispatch('fieldsView/setOpenedGroupIds', groupIdsFromFilter);
	};

	return {
		groupFilter,
		setGroupFilter,
		setFiltersFromUrl,
		resetOpenedGroupIds,
	};
};

export { actions, getters, mutations, useFieldsView };

export default createFieldsViewModule;
