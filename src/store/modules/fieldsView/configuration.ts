import { FIELDS_MAP_ID } from '@/constants';
import { FieldRow } from '@/store/types';

export const TableColumns = {
	NAME: 'name',
	PRODUCER_NAME: 'producerName',
	GROUP_NAME: 'groupName',
	STATUS: 'status',
	OFFLINE: 'offline',
	FIND_MARKER: 'findMarker',
};

export const tableHeaders = {
	[TableColumns.NAME]: {
		text: 'Field',
		sortable: true,
		key: TableColumns.NAME,
	},
	[TableColumns.PRODUCER_NAME]: {
		text: 'Producer',
		sortable: true,
		key: TableColumns.PRODUCER_NAME,
	},
	[TableColumns.GROUP_NAME]: {
		text: 'Group',
		sortable: true,
		key: TableColumns.GROUP_NAME,
	},
	[TableColumns.STATUS]: {
		text: 'Status',
		sortable: true,
		key: TableColumns.STATUS,
	},
	[TableColumns.OFFLINE]: {
		text: 'Offline',
		key: TableColumns.OFFLINE,
	},
	[TableColumns.FIND_MARKER]: {
		text: '',
		key: TableColumns.FIND_MARKER,
	},
};

export const getTableCellComponents = () => {
	return {
		[TableColumns.NAME]: {
			component: 'text-cell',
			getProps: (fieldRow: FieldRow) => ({
				text: fieldRow.name,
				linkTo: `/field/${fieldRow.id}`,
			}),
		},
		[TableColumns.PRODUCER_NAME]: {
			component: 'text-cell',
			getProps: (fieldRow: FieldRow) => ({
				text: fieldRow.producerName,
			}),
		},
		[TableColumns.GROUP_NAME]: {
			component: 'text-cell',
			getProps: (fieldRow: FieldRow) => ({
				text: fieldRow.groupName,
			}),
		},
		[TableColumns.STATUS]: {
			component: 'field-status-cell',
			width: 140,
			getProps: (fieldRow: FieldRow) => ({
				...fieldRow.status,
			}),
		},
		[TableColumns.OFFLINE]: {
			component: 'offline-map-button',
			width: 64,
			getProps: (fieldRow: FieldRow) => ({
				fieldId: fieldRow.id,
				showCaption: false,
			}),
		},
		[TableColumns.FIND_MARKER]: {
			component: 'find-marker-button',
			width: 48,
			getProps: (fieldRow: FieldRow) => ({
				mapId: FIELDS_MAP_ID,
				fieldId: fieldRow.id,
				coordinates: fieldRow.lngLat,
			}),
		},
	};
};
