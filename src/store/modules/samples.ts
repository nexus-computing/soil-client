import { computed } from '@vue/composition-api';
import { ActionTree, GetterTree, MutationTree, Store } from 'vuex';
import Vue from 'vue';
import { AsyncState, EditSampleRequest, Record, ResourceState, RootState, Sample, Sampling } from '@/store/types';
import { createBaseModule } from '@/store/utils/createBaseModule';
import { createUseAllResourceHook } from '@/store/utils/createUseAllResourceHook';
import soilApiService from '../../services/soilApiService';

export interface SamplesState extends ResourceState {
	records: Sample[] & Record[];
	isLoading: boolean;
	isLoaded: boolean;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	error: any;
	edit: AsyncState;
}

function createInitialState(): SamplesState {
	return {
		records: [],
		isLoading: false,
		isLoaded: false,
		error: null,
		edit: {
			isLoading: false,
			isLoaded: false,
			error: null,
		},
	};
}

const actions: ActionTree<SamplesState, RootState> = {
	async requestEdit({ commit }, sample: EditSampleRequest): Promise<boolean> {
		commit('requestEdit');
		try {
			await soilApiService.editSample(sample);
			commit('requestEditSuccess');
			return true;
		} catch (error) {
			commit('requestEditError', error);
			return false;
		}
	},
	acknowledgeEditError({ commit }) {
		commit('resetEditRequest');
	},
};

const mutations: MutationTree<SamplesState> = {
	requestEdit(state) {
		Vue.set(state.edit, 'isLoading', true);
		Vue.set(state.edit, 'isLoaded', false);
		Vue.set(state.edit, 'error', null);
	},
	requestEditSuccess(state) {
		Vue.set(state.edit, 'isLoading', false);
		Vue.set(state.edit, 'isLoaded', true);
		Vue.set(state.edit, 'error', null);
	},
	requestEditError(state, error) {
		Vue.set(state.edit, 'isLoading', false);
		Vue.set(state.edit, 'isLoaded', false);
		Vue.set(state.edit, 'error', error);
	},
	resetEditRequest(state) {
		Vue.set(state.edit, 'isLoading', false);
		Vue.set(state.edit, 'error', null);
	},
};

export const getters: GetterTree<SamplesState, RootState> = {
	getIsEditLoading: (state) => state.edit.isLoading,
	getIsEditLoaded: (state) => state.edit.isLoaded,
	getHasEditError: (state) => Boolean(state.edit.error),
	getEditError: (state) => state.edit.error,
	getBySamplingIds: (_, getters, __, rootGetters) => (samplingIds: string[]) => {
		const samplings = rootGetters['samplings/getByIds'](samplingIds);
		const sampleIds = samplings.flatMap((sampling: Sampling) => sampling.results);
		return getters.getByIds(sampleIds);
	},
	getBySamplingCollectionIds: (_, getters, __, rootGetters) => (samplingCollectionIds: string[]) => {
		const samplingIds = rootGetters['samplings/getBySamplingCollectionIds'](samplingCollectionIds).map(
			({ id }: Sampling) => id
		);
		return getters.getBySamplingIds(samplingIds);
	},
};

const baseModule = createBaseModule('samples', createInitialState);

export const useEditSample = (store: Store<RootState>) => {
	const editSample = (sample: EditSampleRequest): Promise<boolean> => store.dispatch('samples/requestEdit', sample);
	const clearError = () => store.dispatch('samples/acknowledgeEditError');
	const isLoading = computed(() => store.getters['samples/getIsEditLoading']);
	const isLoaded = computed(() => store.getters['samples/getIsEditLoaded']);
	const hasError = computed(() => store.getters['samples/getHasEditError']);
	const error = computed(() => store.getters['samples/getEditError']);

	return {
		editSample,
		clearError,
		isLoading,
		isLoaded,
		hasError,
		error,
	};
};

export const useAllSamples = createUseAllResourceHook('samples');

export default {
	...baseModule,
	actions: {
		...baseModule.actions,
		...actions,
	},
	mutations: {
		...baseModule.mutations,
		...mutations,
	},
	getters: {
		...baseModule.getters,
		...getters,
	},
};
