/* eslint-disable @typescript-eslint/no-explicit-any */
import createTestStore from '../../../tests/createTestStore';
import { createMockSample, createMockSampling, createMockSamplingCollection } from '../../../tests/mockGenerators';
import soilApiService from '../../services/soilApiService';
import samplesModule, { useEditSample } from './samples';
import { createAsyncState, createMockSamplesState } from './test-utils';

jest.mock('../../services/soilApiService');

describe('samples module', () => {
	describe('actions', () => {
		let commitSpy: jest.Mock;

		beforeEach(() => {
			commitSpy = jest.fn();
		});

		describe('requestEdit', () => {
			it("commits 'requestEdit' mutation before making request", async () => {
				await (samplesModule.actions.requestEdit as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestEdit');
			});

			it("commits 'requestEditSuccess' mutation on success", async () => {
				(soilApiService.editSample as jest.Mock).mockResolvedValue('ok');

				await (samplesModule.actions.requestEdit as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestEditSuccess');
			});

			it("commits 'requestEditError' mutation on error", async () => {
				const error = new Error('error');
				(soilApiService.editSample as jest.Mock).mockRejectedValueOnce(error);

				await (samplesModule.actions.requestEdit as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestEditError', error);
			});
		});

		describe('acknowledgeEditError', () => {
			it("commits 'resetEditRequest' mutation", async () => {
				await (samplesModule.actions.acknowledgeEditError as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('resetEditRequest');
			});
		});
	});

	describe('mutations', () => {
		describe('requestEdit', () => {
			it('sets isLoading to true, isLoaded to false, error to null', () => {
				samplesModule.state = createMockSamplesState({
					edit: createAsyncState({
						isLoading: false,
						isLoaded: true,
						error: 'error',
					}),
				});

				samplesModule.mutations.requestEdit(samplesModule.state);

				expect(samplesModule.state.edit).toHaveProperty('isLoading', true);
				expect(samplesModule.state.edit).toHaveProperty('isLoaded', false);
				expect(samplesModule.state.edit).toHaveProperty('error', null);
			});
		});

		describe('requestEditSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null', () => {
				samplesModule.state = createMockSamplesState({
					edit: createAsyncState({
						isLoading: true,
						isLoaded: false,
						error: 'error',
					}),
				});

				samplesModule.mutations.requestEditSuccess(samplesModule.state);

				expect(samplesModule.state.edit).toHaveProperty('isLoading', false);
				expect(samplesModule.state.edit).toHaveProperty('isLoaded', true);
				expect(samplesModule.state.edit).toHaveProperty('error', null);
			});
		});

		describe('requestEditError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error ', () => {
				samplesModule.state = createMockSamplesState({
					edit: createAsyncState({
						isLoading: true,
						isLoaded: true,
						error: null,
					}),
				});

				samplesModule.mutations.requestEditError(samplesModule.state, 'error');

				expect(samplesModule.state.edit).toHaveProperty('isLoading', false);
				expect(samplesModule.state.edit).toHaveProperty('isLoaded', false);
				expect(samplesModule.state.edit).toHaveProperty('error', 'error');
			});
		});

		describe('resetEditRequest', () => {
			it('sets isLoading to false and error to null', () => {
				samplesModule.state = createMockSamplesState({
					edit: createAsyncState({
						isLoading: true,
						error: 'error',
					}),
				});

				samplesModule.mutations.resetEditRequest(samplesModule.state);

				expect(samplesModule.state.edit).toHaveProperty('isLoading', false);
				expect(samplesModule.state.edit).toHaveProperty('error', null);
			});
		});
	});

	describe('getters', () => {
		describe('getIsEditLoading', () => {
			it('returns isLoading', () => {
				const state = createMockSamplesState({
					edit: createAsyncState({ isLoading: true }),
				});

				const actual = (samplesModule.getters as any).getIsEditLoading(state);

				expect(actual).toBe(true);
			});
		});

		describe('getIsEditLoaded', () => {
			it('returns isLoaded', () => {
				const state = createMockSamplesState({
					edit: createAsyncState({ isLoaded: true }),
				});

				const actual = (samplesModule.getters as any).getIsEditLoaded(state);

				expect(actual).toBe(true);
			});
		});

		describe('getHasEditError', () => {
			it('returns true if error is not null', () => {
				const state = createMockSamplesState({
					edit: createAsyncState({ error: 'error' }),
				});

				const actual = (samplesModule.getters as any).getHasEditError(state);

				expect(actual).toBe(true);
			});

			it('returns false if error is null', () => {
				const state = createMockSamplesState({
					edit: createAsyncState({ error: null }),
				});

				const actual = (samplesModule.getters as any).getHasEditError(state);

				expect(actual).toBe(false);
			});
		});

		describe('getEditError', () => {
			it('returns error', () => {
				const state = createMockSamplesState({
					edit: createAsyncState({ error: 'error' }),
				});

				const actual = (samplesModule.getters as any).getEditError(state);

				expect(actual).toEqual('error');
			});
		});

		describe('getBySamplingsIds (utilizing samplings.results as the source of truth, NOT sample.resultOf)', () => {
			it('returns samples belonging to samplings with given ids', () => {
				const store = createTestStore();
				const sampling1 = createMockSampling({ id: 'sampling1', results: ['1', '2'] });
				const sampling2 = createMockSampling({ id: 'sampling2', results: ['4'] });
				const sample1 = createMockSample({ id: '1', resultOf: 'sampling1' });
				const sample2 = createMockSample({ id: '2', resultOf: 'sampling1' });
				// sample3 is not referenced by sampling1 and should NOT be returned
				const sample3 = createMockSample({ id: '3', resultOf: 'sampling1' });
				const sample4 = createMockSample({ id: '4', resultOf: 'sampling2' });
				store.replaceState({
					...store.state,
					samplings: {
						records: [sampling1, sampling2],
					},
					samples: {
						records: [sample1, sample2, sample3, sample4],
					},
				});

				const result = store.getters['samples/getBySamplingIds'](['sampling1', 'sampling2']);

				expect(result).toEqual([sample1, sample2, sample4]);
			});
		});

		describe('getBySamplingCollectionIds', () => {
			it('returns samples belonging to samplings belonging to sampling collections with given ids', () => {
				const store = createTestStore();
				const samplingCollection1 = createMockSamplingCollection({ id: 'sc1', members: ['sampling1'] });
				const sampling1 = createMockSampling({ id: 'sampling1', memberOf: 'sampling1', results: ['1', '2'] });
				// sampling2 is not referenced by samplingCollection1 and should NOT be returned
				const sampling2 = createMockSampling({ id: 'sampling2', memberOf: 'sampling1', results: ['4'] });
				const sample1 = createMockSample({ id: '1', resultOf: 'sampling1' });
				const sample2 = createMockSample({ id: '2', resultOf: 'sampling1' });
				// sample3 is not referenced by sampling1 and should NOT be returned
				const sample3 = createMockSample({ id: '3', resultOf: 'sampling1' });
				// sample4 belongs to sampling2 which is not referenced by samplingCollection1, and should NOT be returned
				const sample4 = createMockSample({ id: '4', resultOf: 'sampling2' });
				store.replaceState({
					...store.state,
					samplingCollections: {
						records: [samplingCollection1],
					},
					samplings: {
						records: [sampling1, sampling2],
					},
					samples: {
						records: [sample1, sample2, sample3, sample4],
					},
				});

				const result = store.getters['samples/getBySamplingCollectionIds'](['sc1']);

				expect(result).toEqual([sample1, sample2]);
			});
		});
	});

	describe('hooks', () => {
		describe('useEditSample', () => {
			it('returns computedRefs of samples edit state', () => {
				const store = createTestStore();
				const samples = createMockSamplesState({
					edit: createAsyncState({
						isLoading: false,
						isLoaded: false,
						error: 'edit error',
					}),
				});
				store.replaceState({
					...store.state,
					samples,
				});

				const actual = useEditSample(store);

				expect(actual.isLoading.value).toEqual(false);
				expect(actual.isLoaded.value).toEqual(false);
				expect(actual.hasError.value).toEqual(true);
				expect(actual.error.value).toEqual('edit error');
			});
		});
	});
});
