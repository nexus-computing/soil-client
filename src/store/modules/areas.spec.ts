/* eslint-disable @typescript-eslint/no-explicit-any */
import createTestStore from '../../../tests/createTestStore';
import soilApiService from '../../services/soilApiService';
import createAreaModule from './areas';
import { actions, getters, useEditArea } from './areas';
import { createAsyncState, createMockAreaState } from './test-utils';

jest.mock('../../services/soilApiService');

describe('areas module', () => {
	describe('actions', () => {
		let commitSpy: jest.Mock;

		beforeEach(() => {
			commitSpy = jest.fn();
		});

		describe('requestEdit', () => {
			it("commits 'requestEdit' mutation before making request", async () => {
				await (actions.requestEdit as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestEdit');
			});

			it("commits 'requestEditSuccess' mutation on success", async () => {
				(soilApiService.editArea as jest.Mock).mockResolvedValue('ok');

				await (actions.requestEdit as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestEditSuccess');
			});

			it("commits 'requestEditError' mutation on error", async () => {
				const error = new Error('error');
				(soilApiService.editArea as jest.Mock).mockRejectedValueOnce(error);

				await (actions.requestEdit as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestEditError', error);
			});
		});

		describe('acknowledgeEditError', () => {
			it("commits 'resetEditRequest' mutation", async () => {
				await (actions.acknowledgeEditError as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('resetEditRequest');
			});
		});
	});

	describe('mutations', () => {
		let areasModule: any;

		beforeEach(() => {
			areasModule = createAreaModule();
		});

		describe('requestEdit', () => {
			it('sets isLoading to true, isLoaded to false, error to null', () => {
				areasModule.state = createMockAreaState({
					edit: createAsyncState({
						isLoading: false,
						isLoaded: true,
						error: 'error',
					}),
				});

				areasModule.mutations.requestEdit(areasModule.state);

				expect(areasModule.state.edit).toHaveProperty('isLoading', true);
				expect(areasModule.state.edit).toHaveProperty('isLoaded', false);
				expect(areasModule.state.edit).toHaveProperty('error', null);
			});
		});

		describe('requestEditSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null', () => {
				areasModule.state = createMockAreaState({
					edit: createAsyncState({
						isLoading: true,
						isLoaded: false,
						error: 'error',
					}),
				});

				areasModule.mutations.requestEditSuccess(areasModule.state);

				expect(areasModule.state.edit).toHaveProperty('isLoading', false);
				expect(areasModule.state.edit).toHaveProperty('isLoaded', true);
				expect(areasModule.state.edit).toHaveProperty('error', null);
			});
		});

		describe('requestEditError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error ', () => {
				areasModule.state = createMockAreaState({
					edit: createAsyncState({
						isLoading: true,
						isLoaded: true,
						error: null,
					}),
				});

				areasModule.mutations.requestEditError(areasModule.state, 'error');

				expect(areasModule.state.edit).toHaveProperty('isLoading', false);
				expect(areasModule.state.edit).toHaveProperty('isLoaded', false);
				expect(areasModule.state.edit).toHaveProperty('error', 'error');
			});
		});

		describe('resetEditRequest', () => {
			it('sets isLoading to false and error to null', () => {
				areasModule.state = createMockAreaState({
					edit: createAsyncState({
						isLoading: true,
						error: 'error',
					}),
				});

				areasModule.mutations.resetEditRequest(areasModule.state);

				expect(areasModule.state.edit).toHaveProperty('isLoading', false);
				expect(areasModule.state.edit).toHaveProperty('error', null);
			});
		});
	});

	describe('getters', () => {
		describe('getIsEditLoading', () => {
			it('returns isLoading', () => {
				const state = createMockAreaState({
					edit: createAsyncState({ isLoading: true }),
				});

				const actual = (getters as any).getIsEditLoading(state);

				expect(actual).toBe(true);
			});
		});

		describe('getIsEditLoaded', () => {
			it('returns isLoaded', () => {
				const state = createMockAreaState({
					edit: createAsyncState({ isLoaded: true }),
				});

				const actual = (getters as any).getIsEditLoaded(state);

				expect(actual).toBe(true);
			});
		});

		describe('getHasEditError', () => {
			it('returns true if error is not null', () => {
				const state = createMockAreaState({
					edit: createAsyncState({ error: 'error' }),
				});

				const actual = (getters as any).getHasEditError(state);

				expect(actual).toBe(true);
			});

			it('returns false if error is null', () => {
				const state = createMockAreaState({
					edit: createAsyncState({ error: null }),
				});

				const actual = (getters as any).getHasEditError(state);

				expect(actual).toBe(false);
			});
		});

		describe('getEditError', () => {
			it('returns error', () => {
				const state = createMockAreaState({
					edit: createAsyncState({ error: 'error' }),
				});

				const actual = (getters as any).getEditError(state);

				expect(actual).toEqual('error');
			});
		});
	});

	describe('hooks', () => {
		describe('useEditArea', () => {
			it('returns computedRefs of areas state', () => {
				const store = createTestStore();
				const areas = createMockAreaState({
					edit: createAsyncState({
						isLoading: false,
						isLoaded: false,
						error: 'edit error',
					}),
				});
				store.replaceState({
					...store.state,
					areas,
				});

				const actual = useEditArea(store);

				expect(actual.isLoading.value).toEqual(false);
				expect(actual.isLoaded.value).toEqual(false);
				expect(actual.hasError.value).toEqual(true);
				expect(actual.error.value).toEqual('edit error');
			});
		});
	});
});
