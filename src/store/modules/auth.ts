/* eslint-disable @typescript-eslint/no-explicit-any */

import { GetterTree } from 'vuex';
import { AuthService, GroupService, MembershipService, SuperAdminService } from '@/services/storageService';
import SurveystackService from '@/services/surveystackService';
import { RootState } from '@/store/types';
export interface User {
	authProviders: any[];
	email: string;
	memberships: any[];
	name: string;
	permissions: string[];
	roles: string[];
	token: string;
	_id: string;
}

export interface AuthState {
	status: string;
	user: User;
	header: string;
}

const deleteCookie = (name: any) => {
	document.cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
};

const deleteCookies = () => {
	deleteCookie('user');
	deleteCookie('token');
};

const createInitialState = (): AuthState => ({
	status: AuthService.getStatus(),
	user: AuthService.getUser(),
	header: AuthService.getHeader(),
});

const initialState = createInitialState();

const getters: GetterTree<AuthState, RootState> = {
	authStatus: (state: AuthState) => state.status,
	isLoggedIn: (state: AuthState) => state.status === 'success',
	isAdmin: (state: AuthState) => state.user && state.user.permissions && state.user.permissions.includes('admin'),
	isSuperAdmin: (state: AuthState) =>
		state.user && state.user.permissions && state.user.permissions.includes('super-admin'),
	user: (state: AuthState) => state.user,
	getAuthorizationHeaderValue: (state: AuthState) => state.header,
	getRoles: (_, getters) => getters.user?.roles ?? [],
};

const clearLocalData = () => {
	// remove items from storage
	AuthService.clear();
	MembershipService.clear();
	GroupService.clear();
	SuperAdminService.clear();
};

const actions = {
	login({ commit }: any, auth: any) {
		return new Promise((resolve, reject) => {
			commit('authRequest');
			SurveystackService.post(auth.url, auth.user)
				.then((resp: any) => {
					const user = resp.data;
					const { email, token } = user;
					const header = `${email} ${token}`;

					AuthService.saveStatus('success');
					AuthService.saveUser(user);
					AuthService.saveHeader(header);

					commit('authSuccess', { user, header });
					resolve(resp.data);
				})
				.catch((err: any) => {
					console.error(err);
					commit('authError');
					reject(err);
				});
		});
	},
	logout({ commit }: any) {
		return new Promise<void>((resolve) => {
			clearLocalData();
			deleteCookies();
			commit('logout');
			resolve();
		});
	},
};

const mutations = {
	authRequest(state: AuthState) {
		state.status = 'loading';
	},
	authSuccess(state: AuthState, { user, header }: any) {
		state.status = 'success';
		state.user = user;
		state.header = header;
		SurveystackService.setHeader('Authorization', header);
	},
	authError(state: AuthState) {
		state.status = 'error';
		state.user = {} as User;
		state.header = '';
	},
	logout(state: AuthState) {
		state.status = '';
		state.user = {} as User;
		state.header = '';
		SurveystackService.removeHeaders();
	},
};

export default {
	namespaced: true,
	state: initialState,
	getters,
	actions,
	mutations,
};
