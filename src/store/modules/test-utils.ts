import { AsyncState } from '../types';
import { AreaState } from './areas';
import { FieldState } from './fields';
import { GroupsState } from './group';
import { ResourcesState } from './resources';
import { SamplesState } from './samples';

export const createAsyncState = ({
	isLoading = false,
	isLoaded = false,
	error = null,
}: Partial<AsyncState> = {}): AsyncState => ({
	isLoading,
	isLoaded,
	error,
});

export const createMockResourcesState = ({
	isLoading = false,
	isLoaded = false,
	error = null,
	submitIsLoading = false,
	submitError = null,
	fields = [],
	areas = [],
	stratifications = [],
	locationCollections = [],
	locations = [],
	samples = [],
	samplings = [],
	samplingCollections = [],
}: Partial<ResourcesState> = {}): ResourcesState => ({
	isLoading,
	isLoaded,
	error,
	submitIsLoading,
	submitError,
	fields,
	areas,
	stratifications,
	locationCollections,
	locations,
	samples,
	samplings,
	samplingCollections,
});

export const createMockGroupsState = ({
	isLoading = false,
	isLoaded = false,
	error = null,
	groups = [],
	groupTree = [],
}: Partial<GroupsState> = {}): GroupsState => ({
	isLoading,
	isLoaded,
	error,
	groups,
	groupTree,
});

export const createMockFieldState = (state: Partial<FieldState> = {}): FieldState => {
	return {
		delete: state.delete ?? createAsyncState(),
		edit: state.edit ?? createAsyncState(),
		create: state.create ?? createAsyncState(),
	};
};

export const createMockAreaState = (state: Partial<AreaState> = {}): AreaState => {
	return {
		edit: state.edit ?? createAsyncState(),
	};
};

export const createMockSamplesState = (state: Partial<SamplesState> = {}): SamplesState => {
	const { isLoaded, isLoading, error } = createAsyncState();
	return {
		records: state.records ?? [],
		isLoading: state.isLoading ?? isLoading,
		isLoaded: state.isLoaded ?? isLoaded,
		error: state.error ?? error,
		edit: state.edit ?? { isLoaded, isLoading, error },
	};
};
