import { computed } from '@vue/composition-api';
import { ActionTree, GetterTree, Module, MutationTree, Store } from 'vuex';
import Vue from 'vue';
import soilApiService from '@/services/soilApiService';
import { AsyncState, CreateFieldRequest, EditFieldRequest, RootState } from '@/store/types';

export interface FieldState {
	edit: AsyncState;
	delete: AsyncState;
	create: AsyncState;
}

function createInitialState() {
	return {
		edit: {
			isLoading: false,
			isLoaded: false,
			error: null,
		},
		delete: {
			isLoading: false,
			isLoaded: false,
			error: null,
		},
		create: {
			isLoading: false,
			isLoaded: false,
			error: null,
		},
	};
}

const actions: ActionTree<FieldState, RootState> = {
	async requestDelete({ commit }, fieldId: string): Promise<boolean> {
		commit('requestDelete');
		try {
			await soilApiService.deleteField(fieldId);
			commit('requestDeleteSuccess');
			return true;
		} catch (error) {
			commit('requestDeleteError', error);
			return false;
		}
	},
	acknowledgeDeleteError({ commit }) {
		commit('resetDeleteRequest');
	},
	async requestEdit({ commit }, field: EditFieldRequest): Promise<boolean> {
		commit('requestEdit');
		try {
			await soilApiService.editField(field);
			commit('requestEditSuccess');
			return true;
		} catch (error) {
			commit('requestEditError', error);
			return false;
		}
	},
	acknowledgeEditError({ commit }) {
		commit('resetEditRequest');
	},
	async requestCreate({ commit }, field: CreateFieldRequest): Promise<boolean> {
		commit('requestCreate');
		try {
			await soilApiService.createField(field);
			commit('requestCreateSuccess');
			return true;
		} catch (error) {
			commit('requestCreateError', error);
			return false;
		}
	},
	acknowledgeCreateError({ commit }) {
		commit('resetCreateRequest');
	},
};

const mutations: MutationTree<FieldState> = {
	requestDelete(state) {
		Vue.set(state.delete, 'isLoading', true);
		Vue.set(state.delete, 'isLoaded', false);
		Vue.set(state.delete, 'error', null);
	},
	requestDeleteSuccess(state) {
		Vue.set(state.delete, 'isLoading', false);
		Vue.set(state.delete, 'isLoaded', true);
		Vue.set(state.delete, 'error', null);
	},
	requestDeleteError(state, error) {
		Vue.set(state.delete, 'isLoading', false);
		Vue.set(state.delete, 'isLoaded', false);
		Vue.set(state.delete, 'error', error);
	},
	resetDeleteRequest(state) {
		Vue.set(state.delete, 'isLoading', false);
		Vue.set(state.delete, 'error', null);
	},
	requestEdit(state) {
		Vue.set(state.edit, 'isLoading', true);
		Vue.set(state.edit, 'isLoaded', false);
		Vue.set(state.edit, 'error', null);
	},
	requestEditSuccess(state) {
		Vue.set(state.edit, 'isLoading', false);
		Vue.set(state.edit, 'isLoaded', true);
		Vue.set(state.edit, 'error', null);
	},
	requestEditError(state, error) {
		Vue.set(state.edit, 'isLoading', false);
		Vue.set(state.edit, 'isLoaded', false);
		Vue.set(state.edit, 'error', error);
	},
	resetEditRequest(state) {
		Vue.set(state.edit, 'isLoading', false);
		Vue.set(state.edit, 'error', null);
	},
	requestCreate(state) {
		Vue.set(state.create, 'isLoading', true);
		Vue.set(state.create, 'isLoaded', false);
		Vue.set(state.create, 'error', null);
	},
	requestCreateSuccess(state) {
		Vue.set(state.create, 'isLoading', false);
		Vue.set(state.create, 'isLoaded', true);
		Vue.set(state.create, 'error', null);
	},
	requestCreateError(state, error) {
		Vue.set(state.create, 'isLoading', false);
		Vue.set(state.create, 'isLoaded', false);
		Vue.set(state.create, 'error', error);
	},
	resetCreateRequest(state) {
		Vue.set(state.create, 'isLoading', false);
		Vue.set(state.create, 'error', null);
	},
};

const getters: GetterTree<FieldState, RootState> = {
	getIsDeleteLoading: (state) => state.delete.isLoading,
	getIsDeleteLoaded: (state) => state.delete.isLoaded,
	getHasDeleteError: (state) => Boolean(state.delete.error),
	getDeleteError: (state) => state.delete.error,
	getIsEditLoading: (state) => state.edit.isLoading,
	getIsEditLoaded: (state) => state.edit.isLoaded,
	getHasEditError: (state) => Boolean(state.edit.error),
	getEditError: (state) => state.edit.error,
	getIsCreateLoading: (state) => state.create.isLoading,
	getIsCreateLoaded: (state) => state.create.isLoaded,
	getHasCreateError: (state) => Boolean(state.create.error),
	getCreateError: (state) => state.create.error,
};

const createFieldsModule = (): Module<FieldState, RootState> => ({
	namespaced: true,
	actions,
	state: createInitialState(),
	mutations,
	getters,
});

const useCreateField = (store: Store<RootState>) => {
	const createField = (field: CreateFieldRequest): Promise<boolean> => store.dispatch('fields/requestCreate', field);
	const clearError = () => store.dispatch('fields/acknowledgeCreateError');
	const isLoading = computed(() => store.getters['fields/getIsCreateLoading']);
	const isLoaded = computed(() => store.getters['fields/getIsCreateLoaded']);
	const hasError = computed(() => store.getters['fields/getHasCreateError']);
	const error = computed(() => store.getters['fields/getCreateError']);

	return {
		createField,
		clearError,
		isLoading,
		isLoaded,
		hasError,
		error,
	};
};

const useEditField = (store: Store<RootState>) => {
	const editField = (field: EditFieldRequest): Promise<boolean> => store.dispatch('fields/requestEdit', field);
	const clearError = () => store.dispatch('fields/acknowledgeEditError');
	const isLoading = computed(() => store.getters['fields/getIsEditLoading']);
	const isLoaded = computed(() => store.getters['fields/getIsEditLoaded']);
	const hasError = computed(() => store.getters['fields/getHasEditError']);
	const error = computed(() => store.getters['fields/getEditError']);

	return {
		editField,
		clearError,
		isLoading,
		isLoaded,
		hasError,
		error,
	};
};

const useDeleteField = (store: Store<RootState>) => {
	const deleteField = (fieldId: string): Promise<boolean> => store.dispatch('fields/requestDelete', fieldId);
	const clearError = () => store.dispatch('fields/acknowledgeDeleteError');
	const isLoading = computed(() => store.getters['fields/getIsDeleteLoading']);
	const isLoaded = computed(() => store.getters['fields/getIsDeleteLoaded']);
	const hasError = computed(() => store.getters['fields/getHasDeleteError']);
	const error = computed(() => store.getters['fields/getDeleteError']);

	return {
		deleteField,
		clearError,
		isLoading,
		isLoaded,
		hasError,
		error,
	};
};

export { actions, getters, mutations, useCreateField, useDeleteField, useEditField };

export default createFieldsModule;
