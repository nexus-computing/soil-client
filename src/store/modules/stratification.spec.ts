/* eslint-disable @typescript-eslint/no-explicit-any */
import createTestStore from '../../../tests/createTestStore';
import soilApiService from '../../services/soilApiService';
import createStratificationsModule, { actions, getters, useStratifications } from './stratifications';
import { createAsyncState } from './test-utils';

jest.mock('../../services/soilApiService');

describe('stratifications module', () => {
	describe('actions', () => {
		let commitSpy: jest.Mock;

		beforeEach(() => {
			commitSpy = jest.fn();
		});

		describe('requestDelete', () => {
			it("commits 'requestDelete' mutation before making request", async () => {
				await (actions.requestDelete as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestDelete');
			});

			it("commits 'requestDeleteSuccess' mutation on success", async () => {
				(soilApiService.deleteStratification as jest.Mock).mockResolvedValue('ok');

				await (actions.requestDelete as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestDeleteSuccess');
			});

			it("commits 'requestDeleteError' mutation on error", async () => {
				const error = new Error('error');
				(soilApiService.deleteStratification as jest.Mock).mockRejectedValueOnce(error);

				await (actions.requestDelete as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestDeleteError', error);
			});
		});

		describe('acknowledgeDeleteError', () => {
			it("commits 'resetDeleteRequest' mutation", async () => {
				await (actions.acknowledgeDeleteError as any)({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('resetDeleteRequest');
			});
		});
	});

	describe('mutations', () => {
		let stratificationsModule: any;

		beforeEach(() => {
			stratificationsModule = createStratificationsModule();
		});

		describe('requestDelete', () => {
			it('sets isLoading to true, isLoaded to false, error to null', () => {
				stratificationsModule.state = createAsyncState({
					isLoading: false,
					isLoaded: true,
					error: 'error',
				});

				stratificationsModule.mutations.requestDelete(stratificationsModule.state);

				expect(stratificationsModule.state).toHaveProperty('isLoading', true);
				expect(stratificationsModule.state).toHaveProperty('isLoaded', false);
				expect(stratificationsModule.state).toHaveProperty('error', null);
			});
		});

		describe('requestDeleteSuccess', () => {
			it('sets isLoading to false, isLoaded to true, error to null', () => {
				stratificationsModule.state = createAsyncState({
					isLoading: true,
					isLoaded: false,
					error: 'error',
				});

				stratificationsModule.mutations.requestDeleteSuccess(stratificationsModule.state);

				expect(stratificationsModule.state).toHaveProperty('isLoading', false);
				expect(stratificationsModule.state).toHaveProperty('isLoaded', true);
				expect(stratificationsModule.state).toHaveProperty('error', null);
			});
		});

		describe('requestDeleteError', () => {
			it('sets isLoading to false, isLoaded to false, error to the passed in error ', () => {
				stratificationsModule.state = createAsyncState({
					isLoading: true,
					isLoaded: true,
					error: null,
				});

				stratificationsModule.mutations.requestDeleteError(stratificationsModule.state, 'error');

				expect(stratificationsModule.state).toHaveProperty('isLoading', false);
				expect(stratificationsModule.state).toHaveProperty('isLoaded', false);
				expect(stratificationsModule.state).toHaveProperty('error', 'error');
			});
		});

		describe('resetDeleteRequest', () => {
			it('sets isLoading to false and error to null', () => {
				stratificationsModule.state = createAsyncState({
					isLoading: true,
					error: 'error',
				});

				stratificationsModule.mutations.resetDeleteRequest(stratificationsModule.state);

				expect(stratificationsModule.state).toHaveProperty('isLoading', false);
				expect(stratificationsModule.state).toHaveProperty('error', null);
			});
		});
	});

	describe('getters', () => {
		describe('getIsLoading', () => {
			it('returns isLoading', () => {
				const state = createAsyncState({ isLoading: true });

				const actual = (getters as any).getIsLoading(state);

				expect(actual).toBe(true);
			});
		});

		describe('getIsLoaded', () => {
			it('returns isLoaded', () => {
				const state = createAsyncState({ isLoaded: true });

				const actual = (getters as any).getIsLoaded(state);

				expect(actual).toBe(true);
			});
		});

		describe('getHasError', () => {
			it('returns true if error is not null', () => {
				const state = createAsyncState({ error: 'error' });

				const actual = (getters as any).getHasError(state);

				expect(actual).toBe(true);
			});

			it('returns false if error is null', () => {
				const state = createAsyncState({ error: null });

				const actual = (getters as any).getHasError(state);

				expect(actual).toBe(false);
			});
		});

		describe('getError', () => {
			it('returns error', () => {
				const state = createAsyncState({ error: 'error' });

				const actual = (getters as any).getError(state);

				expect(actual).toEqual('error');
			});
		});
	});

	describe('hooks', () => {
		describe('useStratifications', () => {
			it('returns computedRefs of stratifications state', () => {
				const store = createTestStore();
				const stratifications = createAsyncState({
					isLoading: false,
					isLoaded: false,
					error: 'error',
				});
				store.replaceState({
					...store.state,
					stratifications,
				});

				const actual = useStratifications(store);

				expect(actual.isLoading.value).toEqual(false);
				expect(actual.isLoaded.value).toEqual(false);
				expect(actual.hasError.value).toEqual(true);
				expect(actual.error.value).toEqual('error');
			});
		});
	});
});
