import { Group, RawGroup } from '@/store/types';

export const handleize = (str: string) => {
	const handle = str
		.toLowerCase()
		.replace(/[^a-z0-9]+/g, '-')
		.replace(/-$/, '')
		.replace(/^-/, '');
	return handle;
};

/**
 * Get the groups that doesn't have parent
 */
export const getRootGroups = (groups: RawGroup[]): RawGroup[] =>
	groups.filter(({ path }) => {
		const parentSlug = path.split('/').slice(0, -2).join('/') + '/';
		return !groups.some((group) => group.path === parentSlug);
	});

/**
 * Get groups with tree structure from the RawGroup array
 */
export const getGroupTree = (groups: RawGroup[], level = 1, parent?: RawGroup): Group[] => {
	const rootGroups = getRootGroups(groups);
	const childGroups = parent
		? groups.filter((group) => group.path === `${parent.path}${handleize(group.name)}/`)
		: rootGroups;

	return childGroups.map((rawGroup) => {
		const children = getGroupTree(groups, level + 1, rawGroup);
		return { ...rawGroup, level, children };
	});
};

/**
 * Get groups array from the groups tree
 */
export const getGroupArray = (source: Group[]): Group[] => {
	const result: Group[] = [];
	source.forEach((group) => {
		result.push(group, ...getGroupArray(group.children));
	});
	return result;
};
