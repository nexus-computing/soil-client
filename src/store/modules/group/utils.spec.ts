import { RawGroup } from '@/store/types';
import { getGroupArray, getGroupTree, getRootGroups, handleize } from './utils';

const createMockRawGroup = (slug = '/soilstack/'): RawGroup[] => [
	{ id: 'group1', name: 'A', path: `${slug}a/` },
	{ id: 'group2', name: 'A-A', path: `${slug}a/a-a/` },
	{ id: 'group3', name: 'A-A-A', path: `${slug}a/a-a/a-a-a/` },
	{ id: 'group4', name: 'A-A-B', path: `${slug}a/a-a/a-a-b/` },
	{ id: 'group5', name: 'A-B', path: `${slug}a/a-b/` },
	{ id: 'group6', name: 'A-B-A', path: `${slug}a/a-b/a-b-a/` },
	{ id: 'group7', name: 'B', path: `${slug}b/` },
	{ id: 'group8', name: 'B-A', path: `${slug}b/b-a/` },
	{ id: 'group9', name: 'C', path: `${slug}c/` },
];

describe('group utils', () => {
	describe('handleize', () => {
		const slug = handleize('Group Name');

		it('handleized name', () => {
			expect(slug).toBe('group-name');
		});
	});

	describe('getRootGroups', () => {
		it('should return the groups that do not have ancestors', () => {
			const source = [
				{ id: 'group1', name: 'North A', path: `/soilstack/region/north/a/` },
				{ id: 'group2', name: 'North A-A', path: `/soilstack/region/north/a/a-a/` },
				{ id: 'group3', name: 'South A-A', path: `/soilstack/region/south/a/a-a/` },
				{ id: 'group4', name: 'North 2', path: `/soilstack/another-region/north/` },
				{ id: 'group5', name: 'Region', path: `/another-soilstack/region/` },
			];

			const actual = getRootGroups(source);

			expect(actual.length).toBe(4);
			expect(actual).not.toContain(expect.objectContaining({ name: 'North A-A' }));
		});
	});

	describe('getGroupTree', () => {
		const rawGroups = createMockRawGroup();
		const groupTree = getGroupTree(rawGroups);

		it('should returns three groups', () => {
			expect(groupTree.length).toBe(3);
		});

		it('The "group1" should have two children', () => {
			expect(groupTree[0].children.length).toBe(2);
		});

		it('The name of the first child of the first chlid of the first group should be "A-A-A"', () => {
			expect(groupTree[0].children[0].children[0].name).toBe('A-A-A');
		});

		it('The level of the first child of the second group should be 2', () => {
			expect(groupTree[1].children[0].level).toBe(2);
		});
	});

	describe('getGroupArray', () => {
		const rawGroups = createMockRawGroup();
		const groupTree = getGroupTree(rawGroups);
		const groups = getGroupArray(groupTree);

		it('should have same length of rawGroups', () => {
			expect(groups.length).toBe(rawGroups.length);
		});

		it('id of the fifth group should be "group5"', () => {
			expect(groups[4].id).toBe('group5');
		});
	});
});
