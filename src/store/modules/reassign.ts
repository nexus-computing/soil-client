import { computed } from '@vue/composition-api';
import { ActionTree, GetterTree, Module, MutationTree, Store } from 'vuex';
import soilApiService from '@/services/soilApiService';
import { AsyncState, ReassignFieldRequest, RootState } from '@/store/types';

function createInitialState() {
	return {
		isLoading: false,
		isLoaded: false,
		error: null,
	};
}

const actions: ActionTree<AsyncState, RootState> = {
	async requestReassign({ commit }, { fieldId, groupId }: ReassignFieldRequest): Promise<boolean> {
		commit('requestReassign');
		try {
			await soilApiService.reassign({ fieldId, groupId });
			commit('requestReassignSuccess');
			return true;
		} catch (error) {
			commit('requestReassignError', error);
			return false;
		}
	},
	acknowledgeReassignError({ commit }) {
		commit('resetReassignRequest');
	},
};

const mutations: MutationTree<AsyncState> = {
	requestReassign(state) {
		state.isLoading = true;
		state.isLoaded = false;
		state.error = null;
	},
	requestReassignSuccess(state) {
		state.isLoading = false;
		state.isLoaded = true;
		state.error = null;
	},
	requestReassignError(state, error) {
		state.isLoading = false;
		state.isLoaded = false;
		state.error = error;
	},
	resetReassignRequest(state) {
		state.isLoading = false;
		state.error = null;
	},
};

const getters: GetterTree<AsyncState, RootState> = {
	getIsLoading: (state) => state.isLoading,
	getIsLoaded: (state) => state.isLoaded,
	getHasError: (state) => Boolean(state.error),
};

const createReassignModule = (): Module<AsyncState, RootState> => ({
	namespaced: true,
	actions,
	state: createInitialState(),
	mutations,
	getters,
});

const useReassign = (store: Store<RootState>) => {
	const reassignField = (arg: ReassignFieldRequest): Promise<boolean> => store.dispatch('fields/requestReassign', arg);
	const clearError = () => store.dispatch('fields/acknowledgeReassignError');
	const isLoading = computed(() => store.getters['reassign/getIsLoading']);
	const isLoaded = computed(() => store.getters['reassign/getIsLoaded']);
	const hasError = computed(() => store.getters['reassign/getHasError']);

	return {
		reassignField,
		clearError,
		isLoading,
		isLoaded,
		hasError,
	};
};

export { actions, getters, mutations, useReassign };

export default createReassignModule;
