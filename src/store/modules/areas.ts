import { computed } from '@vue/composition-api';
import { ActionTree, GetterTree, Module, MutationTree, Store } from 'vuex';
import Vue from 'vue';
import soilApiService from '@/services/soilApiService';
import { Area, AsyncState, RootState } from '@/store/types';

export interface AreaState {
	edit: AsyncState;
}

function createInitialState() {
	return {
		edit: {
			isLoading: false,
			isLoaded: false,
			error: null,
		},
	};
}

const actions: ActionTree<AreaState, RootState> = {
	async requestEdit({ commit }, area: Area): Promise<boolean> {
		commit('requestEdit');
		try {
			await soilApiService.editArea(area);
			commit('requestEditSuccess');
			return true;
		} catch (error) {
			commit('requestEditError', error);
			return false;
		}
	},
	acknowledgeEditError({ commit }) {
		commit('resetEditRequest');
	},
};

const mutations: MutationTree<AreaState> = {
	requestEdit(state) {
		Vue.set(state.edit, 'isLoading', true);
		Vue.set(state.edit, 'isLoaded', false);
		Vue.set(state.edit, 'error', null);
	},
	requestEditSuccess(state) {
		Vue.set(state.edit, 'isLoading', false);
		Vue.set(state.edit, 'isLoaded', true);
		Vue.set(state.edit, 'error', null);
	},
	requestEditError(state, error) {
		Vue.set(state.edit, 'isLoading', false);
		Vue.set(state.edit, 'isLoaded', false);
		Vue.set(state.edit, 'error', error);
	},
	resetEditRequest(state) {
		Vue.set(state.edit, 'isLoading', false);
		Vue.set(state.edit, 'error', null);
	},
};

const getters: GetterTree<AreaState, RootState> = {
	getIsEditLoading: (state) => state.edit.isLoading,
	getIsEditLoaded: (state) => state.edit.isLoaded,
	getHasEditError: (state) => Boolean(state.edit.error),
	getEditError: (state) => state.edit.error,
};

const createAreasModule = (): Module<AreaState, RootState> => ({
	namespaced: true,
	actions,
	state: createInitialState(),
	mutations,
	getters,
});

const useEditArea = (store: Store<RootState>) => {
	const editArea = (area: Area): Promise<boolean> => store.dispatch('areas/requestEdit', area);
	const clearError = () => store.dispatch('areas/acknowledgeEditError');
	const isLoading = computed(() => store.getters['areas/getIsEditLoading']);
	const isLoaded = computed(() => store.getters['areas/getIsEditLoaded']);
	const hasError = computed(() => store.getters['areas/getHasEditError']);
	const error = computed(() => store.getters['areas/getEditError']);

	return {
		editArea,
		clearError,
		isLoading,
		isLoaded,
		hasError,
		error,
	};
};

export { actions, getters, mutations, useEditArea };

export default createAreasModule;
