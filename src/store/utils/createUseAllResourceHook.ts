import { computed } from '@vue/composition-api';
import { Store } from 'vuex';
import { RootState } from '@/store/types';

export const createUseAllResourceHook = (resource: string) => (store: Store<RootState>) => {
	const isLoading = computed(() => store.getters[`${resource}/getIsLoading`]);
	const isLoaded = computed(() => store.getters[`${resource}/getIsLoaded`]);
	const hasError = computed(() => store.getters[`${resource}/hasError`]);
	const records = computed(() => store.getters[`${resource}/getRecords`]);

	if (!isLoading.value && !isLoaded.value) {
		store.dispatch(`${resource}/getAllRecords`);
	}

	return {
		isLoading,
		isLoaded,
		hasError,
		[resource]: records,
	};
};
