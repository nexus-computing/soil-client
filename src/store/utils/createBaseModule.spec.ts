import { createBaseModule } from './createBaseModule';

const createMockInitialState = () => ({
	records: [],
	isLoading: false,
	isLoaded: false,
	error: false,
});

describe('createBaseModule', () => {
	describe('actions', () => {
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		let baseModule: any;
		let commitSpy: jest.Mock;
		let IDBSMock: { [key: string]: jest.Mock };
		beforeEach(() => {
			commitSpy = jest.fn();
			IDBSMock = {
				getRecord: jest.fn(),
				getAllRecords: jest.fn(),
			};
			baseModule = createBaseModule('mock', createMockInitialState, IDBSMock);
		});

		describe('getRecord', () => {
			it('commits requestGetRecord mutation before making request', async () => {
				await baseModule.actions.getRecord({ commit: commitSpy }, '0');

				expect(commitSpy).toHaveBeenCalledWith('requestGetRecord');
			});

			it('commits requestGetRecordSuccess mutation on success', async () => {
				IDBSMock.getRecord.mockReturnValue({ id: '0' });

				await baseModule.actions.getRecord({ commit: commitSpy }, '0');

				expect(commitSpy).toHaveBeenCalledWith('requestGetRecordSuccess', { id: '0' });
			});

			it('commits requestGetRecordError mutation on error', async () => {
				IDBSMock.getRecord.mockImplementation(() => {
					throw 'error';
				});

				await baseModule.actions.getRecord({ commit: commitSpy }, '0');

				expect(commitSpy).toHaveBeenCalledWith('requestGetRecordError', 'error');
			});
		});

		describe('getAllRecords', () => {
			it('commits requestGetAllRecords mutation before making request', async () => {
				await baseModule.actions.getAllRecords({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetAllRecords');
			});

			it('commits requestGetAllRecordsSuccess mutation on success', async () => {
				IDBSMock.getAllRecords.mockReturnValue([{ id: '0' }]);

				await baseModule.actions.getAllRecords({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetAllRecordsSuccess', [{ id: '0' }]);
			});

			it('commits requestGetAllRecordsError mutation on error', async () => {
				IDBSMock.getAllRecords.mockImplementation(() => {
					throw 'error';
				});

				await baseModule.actions.getAllRecords({ commit: commitSpy });

				expect(commitSpy).toHaveBeenCalledWith('requestGetAllRecordsError', 'error');
			});
		});
	});

	describe('mutations', () => {
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		let baseModule: any;
		beforeEach(() => {
			baseModule = createBaseModule('mock', createMockInitialState);
		});

		const initRequestMutations = ['requestGetRecord', 'requestGetAllRecords'];
		initRequestMutations.forEach((mutation) => {
			describe(mutation, () => {
				it('sets isLoading to true, isLoaded to false, error to null', () => {
					baseModule.state = {
						isLoading: false,
						isLoaded: true,
						error: 'error',
					};

					baseModule.mutations[mutation](baseModule.state);

					expect(baseModule.state).toHaveProperty('isLoading', true);
					expect(baseModule.state).toHaveProperty('isLoaded', false);
					expect(baseModule.state).toHaveProperty('error', null);
				});
			});
		});

		describe('requestGetRecordSuccess', () => {
			it('sets isLoading to false, isLoaded to true, and puts record into state', () => {
				baseModule.state = {
					isLoading: true,
					isLoaded: false,
					records: [],
				};

				baseModule.mutations.requestGetRecordSuccess(baseModule.state, { id: '0' });

				expect(baseModule.state).toHaveProperty('isLoading', false);
				expect(baseModule.state).toHaveProperty('isLoaded', true);
				expect(baseModule.state).toHaveProperty('records', [{ id: '0' }]);
			});

			it('replaces an existing record with the same id if present', () => {
				const existingRecord = { id: '0' };
				const updatedRecord = { id: '0', property: 'updated' };
				baseModule.state = {
					records: [existingRecord],
				};

				baseModule.mutations.requestGetRecordSuccess(baseModule.state, updatedRecord);

				expect(baseModule.state.records.length).toEqual(1);
				expect(baseModule.state.records[0]).toBe(updatedRecord);
			});
		});

		describe('requestGetAllRecordsSuccess', () => {
			it('sets isLoading to false, isLoaded to true, and overwrites all records in state', () => {
				baseModule.state = {
					isLoading: true,
					isLoaded: false,
					records: [{ id: '0' }, { id: '1' }],
				};

				baseModule.mutations.requestGetAllRecordsSuccess(baseModule.state, [{ id: '2' }]);

				expect(baseModule.state).toHaveProperty('isLoading', false);
				expect(baseModule.state).toHaveProperty('isLoaded', true);
				expect(baseModule.state.records.length).toBe(1);
				expect(baseModule.state.records).toEqual([{ id: '2' }]);
			});
		});

		const requestErrorMutations = ['requestGetRecordError', 'requestGetAllRecordsError'];
		requestErrorMutations.forEach((mutation) => {
			describe(mutation, () => {
				it('sets isLoading to false, isLoaded to false, error to the passed in error', () => {
					baseModule.state = {
						isLoading: false,
						isLoaded: true,
						error: null,
					};

					baseModule.mutations[mutation](baseModule.state, 'error');

					expect(baseModule.state).toHaveProperty('isLoading', false);
					expect(baseModule.state).toHaveProperty('isLoaded', false);
					expect(baseModule.state).toHaveProperty('error', 'error');
				});
			});
		});
	});

	describe('getters', () => {
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		let baseModule: any;
		beforeEach(() => {
			baseModule = createBaseModule('mock', createMockInitialState);
		});

		describe('getById', () => {
			it('returns the record matching the given id', () => {
				const state = {
					records: [{ id: '0' }, { id: '1' }],
				};

				const result = baseModule.getters.getById(state)('1');

				expect(result).toEqual({ id: '1' });
			});

			it('returns undefined if no record matches the given id', () => {
				const state = {
					records: [{ id: '0' }, { id: '1' }],
				};

				const result = baseModule.getters.getById(state)('2');

				expect(result).toBe(undefined);
			});
		});

		describe('getByIds', () => {
			it('returns an array of records with ids matching the query', () => {
				const state = {
					records: [{ id: '0' }, { id: '1' }, { id: '2' }],
				};

				const result = baseModule.getters.getByIds(state)(['0', '2']);

				expect(result).toEqual([{ id: '0' }, { id: '2' }]);
			});
		});

		describe('getIsLoaded', () => {
			it('returns isLoaded value', () => {
				const state = { isLoaded: true };

				expect(baseModule.getters.getIsLoaded(state)).toEqual(true);
			});
		});

		describe('getIsLoading', () => {
			it('returns isLoading value', () => {
				const state = { isLoading: true };

				expect(baseModule.getters.getIsLoading(state)).toEqual(true);
			});
		});

		describe('getHasError', () => {
			it('returns true if error is present', () => {
				const state = { error: 'message' };

				expect(baseModule.getters.getHasError(state)).toEqual(true);
			});
		});

		describe('getRecords', () => {
			it('returns records value', () => {
				const state = { records: [{ id: '0' }] };

				expect(baseModule.getters.getRecords(state)).toEqual([{ id: '0' }]);
			});
		});
	});
});
