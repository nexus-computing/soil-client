import { ActionPayload, Store } from 'vuex';
import { FieldsViewState } from '@/store/modules/fieldsView';
import { RootState } from '@/store/types';

const excludeFieldsViewActions = [
	'fieldsView/setAllFilters',
	'fieldsView/setActiveFieldId',
	'fieldsView/clearActiveFieldId',
];

const handleAction = (action: ActionPayload, state: RootState) => {
	// Check the action is `fieldsView` action except setAllFilters (update state from URL)
	if (!action.type.startsWith('fieldsView/') || excludeFieldsViewActions.includes(action.type)) {
		return;
	}

	// Fetch all filter state
	const { query, groupFilter, sortBy, sortDir, statusFilter, recentActivityFilter }: FieldsViewState = state.fieldsView;

	// Config query parameter to be added to the URL
	const params = new URLSearchParams();
	if (query) {
		params.append('query', query);
	}
	if (groupFilter.length > 0) {
		params.append('groupFilter', groupFilter.join(','));
	}
	if (sortBy) {
		params.append('sortBy', sortBy);
	}
	if (sortDir) {
		params.append('sortDir', sortDir);
	}
	if (statusFilter) {
		params.append('statusFilter', statusFilter);
	}
	if (recentActivityFilter) {
		params.append('recentActivityFilter', String(recentActivityFilter));
	}

	// If there's any filter, then add params to URL
	if (Array.from(params).length > 0) {
		history.replaceState(null, '', `/fields?${params}`);
	} else {
		history.replaceState(null, '', '/fields');
	}
};

function fieldsViewToUrl(store: Store<RootState>) {
	store.subscribeAction({
		after: handleAction,
	});
}

export { handleAction };

export default fieldsViewToUrl;
