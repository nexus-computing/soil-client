import { ActionPayload } from 'vuex';
import createTestStore from '../../../tests/createTestStore';
import { FieldsViewState } from '../modules/fieldsView';
import { handleAction } from './fieldsViewToUrl';

const createMockFieldsViewState = (args: Partial<FieldsViewState> = {}): FieldsViewState => ({
	query: null,
	sortBy: null,
	sortDir: null,
	groupFilter: [],
	openedGroupIds: [],
	recentActivityFilter: null,
	statusFilter: null,
	activeFieldId: null,
	...args,
});

describe('filtersViewToUrl plugin', () => {
	const store = createTestStore();

	describe(`fieldsView/setQuery`, () => {
		it('Should not append the param if query is not set', () => {
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState() });
			const payload: ActionPayload = { type: 'fieldsView/setQuery', payload: null };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields');
		});

		it('Should append the encoded param if query is set', () => {
			const query = 'Test query';
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState({ query }) });
			const payload: ActionPayload = { type: 'fieldsView/setQuery', payload: query };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields?query=Test+query');
		});
	});

	describe(`fieldsView/setSortBy`, () => {
		it('Should not append the param if sortBy is not set', () => {
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState() });
			const payload: ActionPayload = { type: 'fieldsView/setSortBy', payload: null };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields');
		});

		it('Should append the encoded param if sortBy is set', () => {
			const sortBy = 'name';
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState({ sortBy }) });
			const payload: ActionPayload = { type: 'fieldsView/setSortBy', payload: sortBy };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields?sortBy=name');
		});
	});

	describe(`fieldsView/setSortDir`, () => {
		it('Should not append the param if sortDir is not set', () => {
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState() });
			const payload: ActionPayload = { type: 'fieldsView/setSortDir', payload: null };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields');
		});

		it('Should append the encoded param if sortDir is set', () => {
			const sortDir = 'DESC';
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState({ sortDir }) });
			const payload: ActionPayload = { type: 'fieldsView/setSortDir', payload: sortDir };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields?sortDir=DESC');
		});
	});

	describe(`fieldsView/changeSort`, () => {
		it('Should not append the param if sortBy & sortDir is not set', () => {
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState() });
			const payload: ActionPayload = { type: 'fieldsView/changeSort', payload: null };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields');
		});

		it('Should append the encoded param if sortyBy & sortDir is set', () => {
			const sortDir = 'DESC';
			store.replaceState({
				...store.state,
				fieldsView: createMockFieldsViewState({ sortBy: 'name', sortDir: 'DESC' }),
			});
			const payload: ActionPayload = { type: 'fieldsView/changeSort', payload: sortDir };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields?sortBy=name&sortDir=DESC');
		});
	});

	describe(`fieldsView/setGroupFilter`, () => {
		it('Should not append the param if groupFilter is not set', () => {
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState() });
			const payload: ActionPayload = { type: 'fieldsView/setGroupFilter', payload: [] };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields');
		});

		it('Should append the encoded param if groupFilter is set', () => {
			const groupFilter = ['group1', 'group2'];
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState({ groupFilter }) });
			const payload: ActionPayload = { type: 'fieldsView/setGroupFilter', payload: groupFilter };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields?groupFilter=group1%2Cgroup2');
		});
	});

	describe(`fieldsView/setStatusFilter`, () => {
		it('Should not append the param if statusFilter is not set', () => {
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState() });
			const payload: ActionPayload = { type: 'fieldsView/setStatusFilter', payload: null };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields');
		});

		it('Should append the encoded param if statusFilter is set', () => {
			const statusFilter = 'posted';
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState({ statusFilter }) });
			const payload: ActionPayload = { type: 'fieldsView/setStatusFilter', payload: statusFilter };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields?statusFilter=posted');
		});
	});

	describe(`fieldsView/setRecentActivityFilter`, () => {
		it('Should not append the param if recentActivityFilter is not set', () => {
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState() });
			const payload: ActionPayload = {
				type: 'fieldsView/setRecentActivityFilter',
				payload: { start: null, end: null },
			};
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields');
		});

		it('Should append the encoded param if recentActivityFilter is set', () => {
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState({ recentActivityFilter: '30' }) });
			const payload: ActionPayload = { type: 'fieldsView/setRecentActivityFilter', payload: '30' };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields?recentActivityFilter=30');
		});

		it('Should append the encoded param if recentActivityFilter is set as custom range', () => {
			const recentActivityFilter = '2022-03-01-2022-05-01';
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState({ recentActivityFilter }) });
			const payload: ActionPayload = { type: 'fieldsView/setRecentActivityFilter', payload: recentActivityFilter };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', `/fields?recentActivityFilter=${recentActivityFilter}`);
		});
	});

	describe(`fieldsView/clearAllFilters`, () => {
		it('Should not append any params', () => {
			store.replaceState({ ...store.state, fieldsView: createMockFieldsViewState() });
			const payload: ActionPayload = { type: 'fieldsView/clearAllFilters', payload: undefined };
			const spy = spyOn(history, 'replaceState');

			handleAction(payload, store.state);

			expect(spy).toHaveBeenCalledWith(null, '', '/fields');
		});
	});
});
