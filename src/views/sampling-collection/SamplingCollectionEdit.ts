import { computed, ComputedRef, defineComponent, Ref, ref } from '@vue/composition-api';
import CollectSamplingButton from '@/components/collect/CollectSamplingButton.vue';
import SamplingList from '@/components/collect/SamplingList.vue';
import CollectMap from '@/components/map/CollectMap.vue';
import { useGroups } from '@/store/modules/group';
import { useAllResources } from '@/store/modules/resources';
import { useAllSamples } from '@/store/modules/samples';
import { useAllSamplingCollections } from '@/store/modules/samplingCollections';
import { useAllSamplings } from '@/store/modules/samplings';
import { Area, Field, Location, Sample, Sampling, SamplingCollection, SamplingCollectionStatus } from '@/store/types';
import {
	download,
	downloadLabInventoryCSV,
	generateRawSamplingCollectionCSV,
	useReactiveSamples,
	useReactiveSamplings,
	useUserLocation,
} from '@/views/sampling-collection/helpers';
import SamplingCollectionSubmit from './SamplingCollectionSubmit.vue';

export default defineComponent({
	props: {
		samplingCollectionId: {
			required: true,
			type: String,
		},
		creating: {
			type: Boolean,
		},
	},
	components: {
		CollectMap,
		CollectSamplingButton,
		SamplingList,
		SamplingCollectionSubmit,
	},
	setup(props, { root }) {
		const {
			isLoaded: samplingCollectionsAreLoaded,
			isLoading: samplingCollectionsAreLoading,
			hasError: samplingCollectionsHaveError,
		} = useAllSamplingCollections(root.$store);
		const {
			isLoaded: samplingsAreLoaded,
			isLoading: samplingsAreLoading,
			hasError: samplingsHaveError,
		} = useAllSamplings(root.$store);
		const { isLoaded: samplesAreLoaded, isLoading: samplesAreLoading, hasError: samplesHaveError } = useAllSamples(
			root.$store
		);
		const {
			isLoaded: resourcesAreLoaded,
			isLoading: resourcesAreLoading,
			hasError: resourcesHaveError,
		} = useAllResources(root.$store);
		const { isLoaded: groupsAreLoaded, isLoading: groupsAreLoading, hasError: groupsHaveError } = useGroups(
			root.$store
		);
		const isLoaded = computed(
			() =>
				samplingCollectionsAreLoaded.value &&
				samplingsAreLoaded.value &&
				samplesAreLoaded.value &&
				resourcesAreLoaded.value &&
				groupsAreLoaded.value
		);
		const isLoading = computed(() =>
			[
				samplingCollectionsAreLoading.value,
				samplingsAreLoading.value,
				samplesAreLoading.value,
				resourcesAreLoading.value,
				groupsAreLoading.value,
			].some(Boolean)
		);
		const hasError = computed(() =>
			[
				samplingCollectionsHaveError.value,
				samplingsHaveError.value,
				samplesHaveError.value,
				resourcesHaveError.value,
				groupsHaveError.value,
			].some(Boolean)
		);

		const progressDialogIsVisible = ref(false);

		const samplingCollectionStatus: ComputedRef<SamplingCollectionStatus> = computed(() =>
			root.$store.getters.getSamplingCollectionStatusBySamplingCollectionId(props.samplingCollectionId)
		);

		const samplingCollection = computed<SamplingCollection>(() => {
			if (samplingCollectionStatus.value === 'SUBMITTED') {
				return root.$store.getters['resources/getSamplingCollectionById'](props.samplingCollectionId);
			} else if (samplingCollectionStatus.value === 'DRAFT') {
				return root.$store.getters['samplingCollections/getById'](props.samplingCollectionId);
			}
			return undefined;
		});

		const field = computed<Field>(() =>
			root.$store.getters['resources/getFieldById'](samplingCollection.value?.featureOfInterest)
		);

		const area = computed<Area>(
			() => root.$store.getters['resources/getAreasByFieldId'](samplingCollection.value?.featureOfInterest)?.[0]
		);

		const locations = computed(() =>
			root.$store.getters['resources/getLocationsByFieldId'](samplingCollection.value?.featureOfInterest)
		);

		const mapLocations = computed(() =>
			locations.value?.map((location: Location, i: number) => ({
				...location,
				properties: {
					...location.properties,
					id: location.id,
					/*
						Use location.properties.label if present, otherwise use 1-based indexes.
						Stratifications prior to 2021-10-25 do not include location.properties.label.
						Defaulting to 1-based indexes if label is not present ensures we maintain
						consistent numbering for stratifications created before the above date.
					*/
					label: location.properties?.label ?? `${i + 1}`,
					markerType: 'LOCATION_INCOMPLETE',
				},
			}))
		);

		const locationsById = computed(() =>
			locations.value.reduce(
				(r: { [key: string]: Location }, location: Location) => ({
					...r,
					[location.id]: location,
				}),
				{}
			)
		);

		const activeLocationId: Ref<null | string> = ref(null);
		const activeLocation: ComputedRef<null | Location> = computed(
			() => (activeLocationId.value && locationsById.value[activeLocationId.value]) || null
		);

		const { samplingsR, samplingsRByLocationId } = useReactiveSamplings(root.$store, props.samplingCollectionId);

		const samplings = computed<Sampling[]>(() => {
			if (samplingCollectionStatus.value === 'SUBMITTED') {
				return root.$store.getters['resources/getSamplingsBySamplingCollectionIds']([props.samplingCollectionId]);
			} else if (samplingCollectionStatus.value === 'DRAFT') {
				return samplingsR.value;
			}
			return [];
		});

		const samplingsByLocationId = computed<{ [key: string]: Sampling }>(() => {
			if (samplingCollectionStatus.value === 'SUBMITTED') {
				return samplings.value.reduce(
					(r: { [key: string]: Sampling }, sampling: Sampling) => ({
						...r,
						[sampling.featureOfInterest]: sampling,
					}),
					{}
				);
			} else if (samplingCollectionStatus.value === 'DRAFT') {
				return samplingsRByLocationId.value;
			}
			return {};
		});

		const { samplesReactive, samplesReactiveByLocation } = useReactiveSamples(root.$store, samplings);

		const samples = computed<Sample[]>(() => {
			if (samplingCollectionStatus.value === 'SUBMITTED') {
				const sampleIds = samplings.value.flatMap((sampling: Sampling) => sampling.results);
				return root.$store.getters['resources/getSamplesByIds'](sampleIds);
			} else if (samplingCollectionStatus.value === 'DRAFT') {
				return samplesReactive.value;
			}
			return [];
		});

		const samplesByLocationId = computed<{ [key: string]: Sample[] }>(() => {
			if (samplingCollectionStatus.value === 'SUBMITTED') {
				return samples.value.reduce((r: { [key: string]: Sample[] }, sample: Sample) => {
					const locationId = samplings.value.find((sampling: Sampling) => sample.resultOf === sampling.id)
						?.featureOfInterest;

					if (typeof locationId === 'string') {
						return {
							...r,
							[locationId]: [...(r[locationId] || []), sample],
						};
					}

					return r;
				}, {});
			} else if (samplingCollectionStatus.value === 'DRAFT') {
				return samplesReactiveByLocation.value;
			}
			return {};
		});

		const activeSampling: ComputedRef<null | Sampling> = computed(
			() => (activeLocationId.value && samplingsByLocationId.value[activeLocationId.value]) || null
		);

		const activeSamples: ComputedRef<Sample[]> = computed(
			() => (activeLocationId.value && samplesByLocationId.value[activeLocationId.value]) || []
		);

		function onLocationClick(id: string) {
			activeLocationId.value = activeLocationId.value === id ? null : id;
		}

		const userLocation = useUserLocation(activeLocationId, activeLocation, area);

		function onSamplingCreate({ sampling: newSampling }: { sampling: Sampling; samples: Sample[] }) {
			samplingCollection.value?.members &&
				root.$store.dispatch('samplingCollections/setRecord', {
					...samplingCollection.value,
					members: [...samplingCollection.value.members, newSampling.id],
					dateModified: new Date().toISOString(),
				});
		}

		async function onSamplingDelete(s: Sampling) {
			if (samplingCollection.value?.members.length) {
				root.$store.dispatch('samplingCollections/setRecord', {
					...samplingCollection.value,
					members: samplingCollection.value.members.filter((m) => m !== s.id),
					dateModified: new Date().toISOString(),
				});
			}
		}

		const locationsWithStatus: ComputedRef<Location[] | null> = computed(() =>
			(mapLocations.value as Location[]).map((l: Location) => ({
				...l,
				properties: {
					...l.properties,
					markerType: samplesByLocationId.value[l.id] ? 'LOCATION_COMPLETE' : 'LOCATION_INCOMPLETE',
				},
			}))
		);

		const progress: ComputedRef<string | null> = computed(() => {
			return `${samplings.value.length || 0} / ${locations.value?.length}`;
		});

		function exportRawSamplingCollection(type = 'json') {
			const data = {
				samplingCollections: samplingCollection.value ? [samplingCollection.value] : [],
				samplings: samplings.value,
				samples: samples.value,
				locations: locations.value,
			};

			const filename = `sampling-collection-${new Date().toISOString().replaceAll(/:|\./g, '-')}.${type.toLowerCase()}`;
			let formattedData: string;
			switch (type) {
				case 'csv':
					formattedData = generateRawSamplingCollectionCSV({
						...data,
						field: field.value,
					});
					break;
				case 'json':
					formattedData = JSON.stringify(data);
					break;
				default:
					throw new TypeError(`exportRawSamplingCollection: unknown type ${type}`);
			}

			download(filename, formattedData, type);
		}

		function exportLabInventory() {
			const samplingCollectionData = {
				samplingCollection: samplingCollection.value,
				samplings: samplings.value,
				samples: samples.value,
				locations: locations.value,
			};

			const includePHandP = root.$store.getters['getIsFieldWithinGroupPath'](field.value.id, '/soilstack/esmc/');

			downloadLabInventoryCSV([{ samplingCollectionData, includePHandP }]);
		}

		return {
			exportLabInventory,
			exportRawSamplingCollection,
			progressDialogIsVisible,
			isLoaded,
			isLoading,
			hasError,
			activeLocationId,
			activeLocation,
			onLocationClick,
			...userLocation,
			mapLocations,
			locationsById,
			samplingCollection,
			samplingCollectionStatus,
			samplings,
			samplingsByLocationId,
			activeSampling,
			samples,
			samplesByLocationId,
			activeSamples,
			onSamplingCreate,
			onSamplingDelete,
			locationsWithStatus,
			progress,
			field,
			area,
		};
	},
});
