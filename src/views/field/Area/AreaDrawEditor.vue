<template>
	<v-card flat>
		<v-card-text class="pa-0 sm:pa-4">
			<mgl-map
				ref="mapRef"
				:controls="controls"
				:bounds="mapBounds"
				:fitBoundsOptions="{ padding: 30 }"
				:eventHandlers="eventHandlers"
				@drawCreate="onDrawUpdate"
				@drawUpdate="onDrawUpdate"
				@drawDelete="onDrawDelete"
				@drawSelectionChange="onDrawSelectionChange"
				@drawActionable="onDrawActionable"
				@drawModeChange="onDrawModeChange"
				@loaded="onMapLoaded"
			/>
		</v-card-text>

		<v-card-actions class="px-4 py-5 d-flex flex-wrap">
			<v-btn
				class="ma-0 pl-2 pr-4"
				color="primary"
				:class="{ activeButton: boundaryControlIsActive }"
				:small="$vuetify.breakpoint.smAndDown"
				@click="onBoundaryControlClick"
				outlined
			>
				<v-icon class="mr-1" :style="{ marginTop: '-2px' }" left>mdi-plus</v-icon> Boundary
			</v-btn>
			<v-btn
				class="ma-0 pl-2 pr-4"
				color="primary"
				:class="{ activeButton: cutoutControlIsActive }"
				:small="$vuetify.breakpoint.smAndDown"
				@click="onCutoutControlClick"
				outlined
			>
				<v-icon class="mr-1" :style="{ marginTop: '-2px' }" left>mdi-plus</v-icon> Cutout
			</v-btn>
			<v-btn
				class="ma-0 px-4"
				color="error"
				:small="$vuetify.breakpoint.smAndDown"
				:disabled="!(deleteControlIsActive && drawSelectedFeatureIds.length > 0)"
				@click="onDeleteControlClick"
				outlined
			>
				Delete
			</v-btn>
			<div class="flex-grow-1" />
			<v-btn
				v-if="cancelButton"
				class="ma-0 px-4"
				:class="{ 'ml-auto': $vuetify.breakpoint.smAndDown }"
				@click="$emit('cancel')"
				depressed
				rounded
			>
				{{ cancelButton }}
			</v-btn>
			<v-btn
				color="primary"
				class="ma-0 px-4"
				:class="{ 'ml-auto': $vuetify.breakpoint.smAndDown && !cancelButton }"
				:disabled="!drawFeaturesIsValid"
				@click="onSubmit"
				depressed
				rounded
			>
				{{ okButton }}
			</v-btn>
		</v-card-actions>
	</v-card>
</template>

<script lang="ts">
import { computed, defineComponent, ref, watch } from '@vue/composition-api';
import MapboxDraw from '@mapbox/mapbox-gl-draw';
import { bbox, centerOfMass, featureCollection } from '@turf/turf';
import { Feature, Polygon } from 'geojson';
import { isString } from 'lodash';
import { Popup } from 'mapbox-gl';
import {
	drawActionableHandler,
	drawCreateHandler,
	drawDeleteHandler,
	drawModeChangeHandler,
	drawSelectionChangeHandler,
	drawUpdateHandler,
} from '@/components/map/mapUtils';
import MglMap from '@/components/map/MglMap.vue';
import { ControlTypes } from '@/components/map/mglUtils';
import { createPopupContent, DrawError, DrawMode, finalizeArea, validateDrawFeatures } from './drawUtils';

export default defineComponent({
	components: {
		MglMap,
	},
	props: {
		value: {
			type: Array as () => Feature<Polygon>[],
			default: () => [],
		},
		okButton: {
			type: String,
			default: 'Next',
		},
		cancelButton: {
			type: String,
		},
	},
	emits: ['input', 'submit', 'cancel'],
	setup(props, { emit }) {
		const drawFeatures = ref<Feature<Polygon>[]>(props.value);
		const mapRef = ref<any>(null);
		const drawMode = ref<DrawMode>(DrawMode.None);
		const boundaryControlIsActive = computed(() => drawMode.value === DrawMode.Boundary);
		const cutoutControlIsActive = computed(() => drawMode.value === DrawMode.Cutout);
		const drawErrors = ref<DrawError[]>([]);
		const deleteControlIsActive = ref<boolean>(false);
		const drawSelectedFeatureIds = ref<string[]>([]);
		const drawControl = ref<MapboxDraw>();

		const drawPopups = ref<{ [id: string]: InstanceType<typeof Popup> }>({});

		const deleteFeatureById = (id: string | number) => {
			drawControl.value?.delete(String(id));
			drawFeatures.value = drawControl.value?.getAll().features as Feature<Polygon>[];
			drawErrors.value = validateDrawFeatures(drawFeatures.value);
			drawMode.value = DrawMode.None;
		};

		watch(drawErrors, () => {
			// calculate centers for each error's feature
			interface ErrorWithCenter {
				id?: string | number;
				reason: string;
				center: [number, number];
			}
			const drawErrorsWithCenters: ErrorWithCenter[] = drawErrors.value.map((error: DrawError) => ({
				...error,
				center: centerOfMass(drawControl.value?.get(String(error.id))).geometry.coordinates as [number, number],
			}));
			const isNewId = ({ id }: ErrorWithCenter) => isString(id) && !Object.keys(drawPopups.value).includes(id);
			const newErrorsWithCenters = drawErrorsWithCenters.filter(isNewId);

			Object.entries(drawPopups.value).forEach(([featureId, popup]) => {
				const error = drawErrorsWithCenters.find(({ id }: ErrorWithCenter) => id === featureId);
				if (error) {
					popup.setLngLat(error.center).addTo(mapRef.value.map);
					return;
				}
				popup.remove();
			});

			newErrorsWithCenters.forEach((error: ErrorWithCenter) => {
				const popup = new Popup({ closeButton: false, closeOnClick: false, maxWidth: 'none' });
				const content = createPopupContent(error.id, error.reason, deleteFeatureById, popup.remove);
				popup.setLngLat(error.center).setDOMContent(content).addTo(mapRef.value.map);
				drawPopups.value[error.id || '_'] = popup;
				popup.addTo(mapRef.value.map);
			});
		});

		type DrawCreateOrUpdateEvent = MapboxDraw.DrawCreateEvent | MapboxDraw.DrawUpdateEvent;
		function onDrawUpdate(ev: DrawCreateOrUpdateEvent) {
			// Add property to features to distinguish polygons between boundaries and cutouts
			const updatedFeaturesWithProps = ev.features.map((feature: Feature) => ({
				...feature,
				properties: {
					...feature.properties,
					type: feature.properties?.type || drawMode.value,
				},
			}));

			const drawControl = mapRef.value.mapControls[ControlTypes.Draw];

			// Update drawControl's FeatureCollection
			drawControl.set({ type: 'FeatureCollection', features: [...drawFeatures.value, ...updatedFeaturesWithProps] });
			drawFeatures.value = drawControl.getAll().features;
			drawErrors.value = validateDrawFeatures(drawFeatures.value);

			drawMode.value = DrawMode.None;
		}

		function onDrawDelete() {
			const drawControl = mapRef.value?.mapControls[ControlTypes.Draw];
			drawFeatures.value = drawControl?.getAll().features;
			drawErrors.value = validateDrawFeatures(drawFeatures.value);
			drawMode.value = DrawMode.None;
		}

		watch(
			() => drawFeatures.value,
			(value) => {
				emit('input', value);
			}
		);

		return {
			mapBounds: drawFeatures.value.length > 0 ? bbox(featureCollection(drawFeatures.value)) : null,
			drawErrors,
			drawPopups,
			mapRef,
			controls: [ControlTypes.Geocoder, ControlTypes.Geolocate, ControlTypes.Draw],
			onSubmit() {
				emit('input', drawFeatures.value);
				emit('submit', finalizeArea(drawFeatures.value as Feature<Polygon>[]));
			},
			onMapLoaded() {
				// Initialize MapboxGL Draw with FeatureCollection from drawFeatures
				drawControl.value = mapRef.value.mapControls[ControlTypes.Draw];
				drawControl.value?.set({
					type: 'FeatureCollection',
					features: drawFeatures.value,
				});
			},
			drawFeatures,
			drawFeaturesIsValid: computed(() => {
				return drawFeatures.value.length > 0 && validateDrawFeatures(drawFeatures.value).length === 0;
			}),
			eventHandlers: [
				drawCreateHandler,
				drawUpdateHandler,
				drawDeleteHandler,
				drawSelectionChangeHandler,
				drawModeChangeHandler,
				drawActionableHandler,
			],
			boundaryControlIsActive,
			cutoutControlIsActive,
			onBoundaryControlClick() {
				if (boundaryControlIsActive.value) {
					drawControl.value?.changeMode('simple_select');
				} else {
					drawControl.value?.changeMode('draw_polygon');
				}
				drawMode.value = drawMode.value === DrawMode.Boundary ? DrawMode.None : DrawMode.Boundary;
			},
			onCutoutControlClick() {
				const drawControl = mapRef.value.mapControls[ControlTypes.Draw];
				const nextMode = cutoutControlIsActive.value ? drawControl.modes.SIMPLE_SELECT : drawControl.modes.DRAW_POLYGON;
				drawControl.changeMode(nextMode);
				drawMode.value = drawMode.value === DrawMode.Cutout ? DrawMode.None : DrawMode.Cutout;
			},
			onDrawUpdate,
			onDrawDelete,
			onDrawSelectionChange({ features }: MapboxDraw.DrawSelectionChangeEvent) {
				const isValidId = (id: string | number | undefined): id is string => {
					return !!id && typeof id === 'string';
				};
				drawSelectedFeatureIds.value = features.map(({ id }) => id).filter(isValidId);
			},
			onDrawActionable({ actions }: MapboxDraw.DrawActionableEvent) {
				deleteControlIsActive.value = actions.trash;
			},
			deleteControlIsActive,
			drawSelectedFeatureIds,
			onDeleteControlClick() {
				drawControl.value?.delete(drawSelectedFeatureIds.value);
				drawFeatures.value = drawControl.value?.getAll().features as Feature<Polygon>[];
				drawErrors.value = validateDrawFeatures(drawFeatures.value);
				drawMode.value = DrawMode.None;
				drawSelectedFeatureIds.value = [];
			},
			onDrawModeChange({ mode }: MapboxDraw.DrawModeChangeEvent) {
				if (mode === MapboxDraw.modes.SIMPLE_SELECT || mode === MapboxDraw.modes.DIRECT_SELECT) {
					drawMode.value = DrawMode.None;
				}
			},
		};
	},
});
</script>

<style lang="scss" scoped>
.activeButton:before,
.activeButton:hover:before {
	opacity: 0.18;
	background-color: 'currentColor';
}

.v-card__actions {
	gap: 8px;

	.flex-grow-1 {
		width: 100%;
	}
}

@media (min-width: 768px) {
	.v-card__actions .flex-grow-1 {
		width: auto;
	}
}
</style>
