import { multiPolygon, polygon } from '@turf/turf';
import { Feature, Geometry, MultiPolygon, Position } from 'geojson';
import { isEqual } from 'lodash';
import { createPopupContent, DrawMode, finalizeArea, unfinalizeArea, validateDrawFeatures } from './drawUtils';

const boundary0Ring: Position[] = [
	[0, 0],
	[4, 0],
	[4, 4],
	[0, 4],
	[0, 0],
];
const boundary0 = polygon([boundary0Ring], { type: DrawMode.Boundary }, { id: 'b0' });

const boundary1Ring: Position[] = [
	[5, 0],
	[9, 0],
	[9, 4],
	[5, 4],
	[5, 0],
];
const boundary1 = polygon([boundary1Ring], { type: DrawMode.Boundary }, { id: 'b1' });

// contained by boundary 1, valid
const cutout1aRing: Position[] = [
	[6, 1],
	[7, 1],
	[7, 2],
	[6, 2],
	[6, 1],
];
const cutout1a = polygon([cutout1aRing], { type: DrawMode.Cutout }, { id: 'c1a' });

// contined by boundary0, valid
const cutout0aRing: Position[] = [
	[1, 1],
	[2, 1],
	[2, 2],
	[1, 2],
	[1, 1],
];
const cutout0a = polygon([cutout0aRing], { type: DrawMode.Cutout }, { id: 'c0a' });

// contained by boundary0, valid
const cutout0bRing: Position[] = [
	[2, 2],
	[3, 2],
	[3, 3],
	[2, 3],
	[2, 2],
];
const cutout0b = polygon([cutout0bRing], { type: DrawMode.Cutout }, { id: 'c0b' });

// overlapping the bottom right edge of boundary0, invalid
const cutout0zRing: Position[] = [
	[3, 3],
	[5, 3],
	[5, 5],
	[3, 5],
	[3, 3],
];
const cutout0z = polygon([cutout0zRing], { type: DrawMode.Cutout }, { id: 'c0z' });

// bisecting boundary0, invalid
const cutout0yRing: Position[] = [
	[1, -1],
	[2, -1],
	[2, 5],
	[1, 5],
	[1, -1],
];
const cutout0y = polygon([cutout0yRing], { type: DrawMode.Cutout }, { id: 'c0y' });

const areaWithMultiplePolygonsAndCutouts: Feature<MultiPolygon> = multiPolygon([
	[boundary0Ring, cutout0aRing, cutout0bRing],
	[boundary1Ring, cutout1aRing],
]);

describe('validateDrawFeatures', () => {
	it('returns empty array for valid features', () => {
		const drawErrors = validateDrawFeatures([boundary0, cutout0a]);
		expect(drawErrors.length).toBe(0);
	});

	it('returns an error for a cutout that exceeds a boundary', () => {
		const drawErrors = validateDrawFeatures([boundary0, cutout0z]);
		expect(drawErrors.length).toBe(1);
		expect(drawErrors[0].id).toBe(cutout0z.id);
	});

	it('returns an error for a cutout that bisects a boundary', () => {
		const drawErrors = validateDrawFeatures([boundary0, cutout0y]);
		expect(drawErrors.length).toBe(1);
		expect(drawErrors[0].id).toBe(cutout0y.id);
	});
});

describe('finalizeArea', () => {
	it('combines features into multipolygon with holes', () => {
		const finalized = finalizeArea([boundary0, cutout0a, cutout0b]);
		expect(finalized);
		expect(finalized.geometry.type).toBe('MultiPolygon');
		expect(finalized.geometry.coordinates[0][0]).toEqual(boundary0.geometry.coordinates[0]);
		expect(finalized.geometry.coordinates[0]).toContain(cutout0a.geometry.coordinates[0]);
		expect(finalized.geometry.coordinates[0]).toContain(cutout0b.geometry.coordinates[0]);
	});

	it('matches holes to corresponding boundaries', () => {
		const finalized = finalizeArea([boundary0, cutout0a, cutout0b, boundary1, cutout1a]);
		expect(finalized.geometry.type).toBe('MultiPolygon');
		expect(finalized.geometry.coordinates[0][0]).toEqual(boundary0.geometry.coordinates[0]);
		expect(finalized.geometry.coordinates[0]).toContainEqual(cutout0a.geometry.coordinates[0]);
		expect(finalized.geometry.coordinates[1][0]).toEqual(boundary1.geometry.coordinates[0]);
		expect(finalized.geometry.coordinates[1]).toContainEqual(cutout1a.geometry.coordinates[0]);
	});
});

describe('unfinalizeArea', () => {
	it('flattens MultiPolygon Feature into list of Polygon Features, labeled as cutouts and boundaries and set unique IDs', () => {
		const drawFeatures = unfinalizeArea(areaWithMultiplePolygonsAndCutouts);
		const flatDrawCoords = drawFeatures.map((feature) => feature.geometry.coordinates[0]);
		expect(flatDrawCoords).toContainEqual(boundary0.geometry.coordinates[0]);

		const findByGeometry = (geo: Geometry) => drawFeatures.find((feature: Feature) => isEqual(feature.geometry, geo));
		const foundBoundary0 = findByGeometry(boundary0.geometry);
		const foundBoundary1 = findByGeometry(boundary1.geometry);
		const foundCutout0a = findByGeometry(cutout0a.geometry);
		const foundCutout0b = findByGeometry(cutout0b.geometry);
		const foundCutout1a = findByGeometry(cutout1a.geometry);
		expect(foundBoundary0?.properties?.type).toBe(DrawMode.Boundary);
		expect(foundBoundary0?.id).toBe('Boundary-0');
		expect(foundBoundary1?.properties?.type).toBe(DrawMode.Boundary);
		expect(foundBoundary1?.id).toBe('Boundary-1');
		expect(foundCutout0a?.properties?.type).toBe(DrawMode.Cutout);
		expect(foundCutout0a?.id).toBe('Cutout-0');
		expect(foundCutout0b?.properties?.type).toBe(DrawMode.Cutout);
		expect(foundCutout0b?.id).toBe('Cutout-1');
		expect(foundCutout1a?.properties?.type).toBe(DrawMode.Cutout);
		expect(foundCutout1a?.id).toBe('Cutout-2');
	});
});

describe('createPopupContent', () => {
	it('displays reason', () => {
		const reason = 'my error reason';
		const popupContent = createPopupContent('featureId', reason, jest.fn, jest.fn);
		expect(popupContent.querySelector('.reason')?.innerHTML).toBe(reason);
	});
	it('clicking delete button calls deleteFeatureHandler and popupRemoveHandler', () => {
		const deleteFeatureHandler = jest.fn();
		const popupRemoveHandler = jest.fn();
		const popupContent = createPopupContent('featureId', 'reason', deleteFeatureHandler, popupRemoveHandler);
		popupContent.querySelector('button')?.click();
		expect(deleteFeatureHandler).toHaveBeenCalled();
		expect(popupRemoveHandler).toHaveBeenCalled();
	});
});
