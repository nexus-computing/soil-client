import { createMockArea, createMockMultiPolygon } from '../../../../tests/mockGenerators/index';
import { renderWithVuetify } from '../../../../tests/renderWithVuetify';
import AreaDrawReview from './AreaDrawReview.vue';

const createMockDrawnArea = () => createMockArea({ geometry: createMockMultiPolygon() });

describe('AreaDrawReview', () => {
	it('renders with next and back buttons', () => {
		const { getByText } = renderWithVuetify(AreaDrawReview, {
			propsData: {
				area: createMockDrawnArea(),
			},
			stubs: {
				MglMap: {
					template: '<span class="mgl-map" />',
				},
			},
		});
		getByText('Submit');
		getByText('Back');
	});
	it('emits submit event when submit button is clicked', async () => {
		const { getByText, emitted } = renderWithVuetify(AreaDrawReview, {
			propsData: {
				area: createMockDrawnArea(),
			},
			stubs: {
				MglMap: {
					template: '<span class="mgl-map" />',
				},
			},
		});
		await getByText('Submit').click();
		expect(emitted()).toHaveProperty('submit');
	});
	it('emits back event when back button is clicked', async () => {
		const { getByText, emitted } = renderWithVuetify(AreaDrawReview, {
			propsData: {
				area: createMockDrawnArea(),
			},
			stubs: {
				MglMap: {
					template: '<span class="mgl-map" />',
				},
			},
		});
		await getByText('Back').click();
		expect(emitted()).toHaveProperty('back');
	});
});
