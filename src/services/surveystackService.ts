/* eslint-disable @typescript-eslint/no-explicit-any */

import axios from 'axios';
import { AuthService } from './storageService';

const httpClient = axios.create();

const SurveystackService: any = {
	init(baseURL: any) {
		httpClient.defaults.baseURL = baseURL;
		const status = AuthService.getStatus();
		if (status === 'success') {
			const header = AuthService.getHeader();
			this.setHeader('Authorization', header);
		}
	},

	setHeader(name: any, value: any) {
		httpClient.defaults.headers.common[name] = value;
	},

	removeHeaders() {
		httpClient.defaults.headers.common = {};
	},

	removeHeader(name: any) {
		delete httpClient.defaults.headers.common[name];
	},

	get(resource: any) {
		return httpClient.get(resource);
	},

	post(resource: any, data: any) {
		return httpClient.post(resource, data);
	},

	put(resource: any, data: any) {
		return httpClient.put(resource, data);
	},

	delete(resource: any) {
		return httpClient.delete(resource);
	},

	/**
	 * Perform a custom Axios request.
	 * */
	customRequest(data: any) {
		return httpClient(data);
	},
};

export default SurveystackService;
