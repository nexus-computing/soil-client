import { Store } from 'vuex';
import axios from 'axios';
import {
	Area,
	CreateFieldRequest,
	EditFieldRequest,
	EditSampleRequest,
	ReassignFieldRequest,
	RootState,
} from '@/store/types';
import { SuperAdminService } from './storageService';

let _store: Store<RootState>;

const getAllResources = async () => {
	const response = await axios.get(`${process.env.VUE_APP_SOIL_API_URL}/resources`, {
		headers: {
			'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
		},
	});
	return response?.data;
};

const submitSamplingCollection = async (populatedSamplingCollection: object, groupId: string) => {
	const response = await axios.post(
		`${process.env.VUE_APP_SOIL_API_URL}/sampling-collections?groupId=${groupId}`,
		populatedSamplingCollection,
		{
			headers: {
				'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
			},
		}
	);
	return response?.data;
};

const getGroups = async () => {
	const response = await axios.get(`${process.env.VUE_APP_SOIL_API_URL}/groups`, {
		headers: {
			'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
		},
	});
	return response?.data;
};

const reassign = async ({ groupId, fieldId }: ReassignFieldRequest) =>
	await axios.patch(
		`${process.env.VUE_APP_SOIL_API_URL}/reassign`,
		{ groupId, fieldId },
		{
			headers: {
				'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
			},
		}
	);

const createField = async ({ groupId, ...field }: CreateFieldRequest) =>
	await axios.post(`${process.env.VUE_APP_SOIL_API_URL}/fields?groupId=${groupId}`, field, {
		headers: {
			'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
		},
	});

const editField = async ({ id, ...field }: EditFieldRequest) =>
	await axios.patch(`${process.env.VUE_APP_SOIL_API_URL}/fields/${id}`, field, {
		headers: {
			'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
		},
	});

const deleteField = async (fieldId: string) =>
	await axios.delete(`${process.env.VUE_APP_SOIL_API_URL}/fields/${fieldId}`, {
		headers: {
			'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
			'X-Super-Authorization': SuperAdminService.getPassword(),
		},
	});

const deleteStratification = async (stratificationId: string) =>
	await axios.delete(`${process.env.VUE_APP_SOIL_API_URL}/stratifications/${stratificationId}`, {
		headers: {
			'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
			'X-Super-Authorization': SuperAdminService.getPassword(),
		},
	});

const editArea = async ({ id, ...area }: Area) =>
	await axios.patch(`${process.env.VUE_APP_SOIL_API_URL}/areas/${id}`, area, {
		headers: {
			'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
		},
	});

const editSample = async ({ id, ...sample }: EditSampleRequest) =>
	await axios.patch(`${process.env.VUE_APP_SOIL_API_URL}/samples/${id}`, sample, {
		headers: {
			'X-Authorization': _store.getters['auth/getAuthorizationHeaderValue'],
		},
	});

const injectStore = (store: Store<RootState>) => {
	_store = store;
};

const soilApiService = {
	injectStore,
	getAllResources,
	submitSamplingCollection,
	getGroups,
	reassign,
	createField,
	editField,
	deleteField,
	deleteStratification,
	editArea,
	editSample,
};

export default soilApiService;
