export default function debounce(func: Function, wait: number): any {
	let timeout: any;
	return function (...args: any) {
		const later = function (this: any) {
			timeout = null;
			func.apply(this, args);
		};
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
	};
}
