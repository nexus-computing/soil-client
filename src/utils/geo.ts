import { booleanPointInPolygon, centerOfMass, centroid } from '@turf/turf';
import { Feature, MultiPolygon, Point, Polygon } from 'geojson';

/*
 centerOfMass returns a nice center that looks good visually for most fields.
 However, the shape of some fields means that their centerOfMass is outside the field bounds.
 This doesn't look good for map marker placement, or work well as a point to pass to gps navigation.
 So, if the centerOfMass is outside the field bounds, we fallback to using the centroid,
 which will always be within the field bounds but may not be at the "visual center".
*/
function prettyGoodCenterOf(area: Feature<Polygon | MultiPolygon>): Feature<Point | null, any> {
	const center = centerOfMass(area);
	return booleanPointInPolygon(center, area) ? center : centroid(area);
}

export { prettyGoodCenterOf };
