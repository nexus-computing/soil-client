import { hasMinimumLength, isNotEmpty, isValidEmailAddress } from './rules';

describe('rules utils', () => {
	describe('hasMinimumLength', () => {
		it('returns true if the value is greater than given length in length', () => {
			const isValid = hasMinimumLength(3)('1234');
			expect(isValid).toBe(true);
		});

		it('returns true if the value is equal to given length in length', () => {
			const isValid = hasMinimumLength(3)('abc');
			expect(isValid).toBe(true);
		});

		it('returns false if the value is less than given length in length', () => {
			const isValid = hasMinimumLength(10)('abcdef123');
			expect(isValid).toBe(false);
		});

		it('returns given error message if the value is less than given length in length', () => {
			const isValid = hasMinimumLength(50, 'at least 4 characters long')('abc123');
			expect(isValid).toBe('at least 4 characters long');
		});
	});

	describe('isNotEmpty ', () => {
		it('returns true if the value is set', () => {
			const isValid = isNotEmpty()('any value');
			expect(isValid).toBe(true);
		});

		it('returns false if the value is empty', () => {
			const isValid = isNotEmpty()('');
			expect(isValid).toBe(false);
		});

		it('returns given error message if the value is empty', () => {
			const isValid = isNotEmpty('please enter your name')('');
			expect(isValid).toBe('please enter your name');
		});
	});

	describe('isValidEmailAddress', () => {
		it('returns true if the value is valid email address', () => {
			const isValid = isValidEmailAddress()('is.valid@email.com');
			expect(isValid).toBe(true);
		});

		it('returns false if the value is not valid email address', () => {
			let isValid = isValidEmailAddress()('');
			expect(isValid).toBe(false);

			isValid = isValidEmailAddress()('test');
			expect(isValid).toBe(false);

			isValid = isValidEmailAddress()('test@');
			expect(isValid).toBe(false);

			isValid = isValidEmailAddress()('test@test');
			expect(isValid).toBe(false);

			isValid = isValidEmailAddress()('@wrong');
			expect(isValid).toBe(false);

			isValid = isValidEmailAddress()('in valid@email.com');
			expect(isValid).toBe(false);
		});

		it('returns given error message if the value is not a valid email address', () => {
			const isValid = isValidEmailAddress('should be valid email address')('wrong email');
			expect(isValid).toBe('should be valid email address');
		});
	});
});
