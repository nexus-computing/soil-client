export const hasMinimumLength = (length = 4, message?: string) => (val: string): boolean | string => {
	return val.length >= length || message || false;
};

export const isNotEmpty = (message?: string) => (val: string): boolean | string => {
	return !!val || message || false;
};

export const isValidEmailAddress = (message?: string) => (val: string): boolean | string => {
	const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return regex.test(val.toLowerCase()) || message || false;
};
