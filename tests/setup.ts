import CompositionApi from '@vue/composition-api';
import Vuetify from 'vuetify';
import Vue from 'vue';
import '@testing-library/jest-dom';

window.URL.createObjectURL = function () {
	return '';
};

Date.prototype.getTimezoneOffset = function () {
	return 0;
};

Vue.use(Vuetify);
Vue.use(CompositionApi);
