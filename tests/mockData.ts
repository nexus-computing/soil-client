export const locations = [
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e40',
			label: '1',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49357422255152, 39.11823382556361],
		},
		id: '5f9840bb6dc285fd63d96e40',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e41',
			label: '2',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49405257976026, 39.1182316249952],
		},
		id: '5f9840bb6dc285fd63d96e41',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e42',
			label: '3',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49371999700408, 39.11823866507521],
		},
		id: '5f9840bb6dc285fd63d96e42',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e43',
			label: '4',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49376622357111, 39.118268131554565],
		},
		id: '5f9840bb6dc285fd63d96e43',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e44',
			label: '5',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49450508480619, 39.11807636824945],
		},
		id: '5f9840bb6dc285fd63d96e44',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e45',
			label: '6',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49484745133272, 39.11806523841645],
		},
		id: '5f9840bb6dc285fd63d96e45',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e46',
			label: '7',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49486011172472, 39.118306796646415],
		},
		id: '5f9840bb6dc285fd63d96e46',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e47',
			label: '8',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49526229823188, 39.11839515233418],
		},
		id: '5f9840bb6dc285fd63d96e47',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e48',
			label: '9',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49456755795731, 39.11815759882434],
		},
		id: '5f9840bb6dc285fd63d96e48',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e49',
			label: '10',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49463114958283, 39.118164735328726],
		},
		id: '5f9840bb6dc285fd63d96e49',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e4a',
			label: '11',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49568974267908, 39.11830666449404],
		},
		id: '5f9840bb6dc285fd63d96e4a',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e4b',
			label: '12',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.4941132087159, 39.118276420150394],
		},
		id: '5f9840bb6dc285fd63d96e4b',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e4c',
			label: '13',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.4941790547087, 39.118034053664445],
		},
		id: '5f9840bb6dc285fd63d96e4c',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e4d',
			label: '14',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49474351137438, 39.118242504091505],
		},
		id: '5f9840bb6dc285fd63d96e4d',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e4e',
			label: '15',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49582728660741, 39.118340685723034],
		},
		id: '5f9840bb6dc285fd63d96e4e',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e4f',
			label: '16',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49406224714147, 39.117988920877686],
		},
		id: '5f9840bb6dc285fd63d96e4f',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e50',
			label: '17',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49451619435231, 39.11833658458082],
		},
		id: '5f9840bb6dc285fd63d96e50',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e51',
			label: '18',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49469939756139, 39.11832195135303],
		},
		id: '5f9840bb6dc285fd63d96e51',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e52',
			label: '19',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.4941729259757, 39.118061784038076],
		},
		id: '5f9840bb6dc285fd63d96e52',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e53',
			label: '20',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49530243424611, 39.11830615577261],
		},
		id: '5f9840bb6dc285fd63d96e53',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e54',
			label: '21',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49458828357504, 39.11815046630542],
		},
		id: '5f9840bb6dc285fd63d96e54',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e55',
			label: '22',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49506533651659, 39.11834489554804],
		},
		id: '5f9840bb6dc285fd63d96e55',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e56',
			label: '23',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49388975264385, 39.11805078558798],
		},
		id: '5f9840bb6dc285fd63d96e56',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e57',
			label: '24',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49369032695768, 39.118213546536175],
		},
		id: '5f9840bb6dc285fd63d96e57',
	},
	{
		type: 'Feature',
		properties: {
			id: '5f9840bb6dc285fd63d96e58',
			label: '25',
			markerType: 'LOCATION_INCOMPLETE',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.49460930759071, 39.11820577958814],
		},
		id: '5f9840bb6dc285fd63d96e58',
	},
];

export const userLocation = {
	coords: {
		accuracy: 150,
		latitude: 39.1186523358723,
		longitude: -84.49588990912653,
	},
	timestamp: 1605209374244,
};

export const samplings = [
	{
		id: '5fb3e14c04db82000118ffd5',
		featureOfInterest: '5f9840bb6dc285fd63d96e4a',
		geometry: {
			type: 'Point',
			coordinates: [-84.49588990912653, 39.1186523358723],
		},
		property: { geoAccuracy: 150 },
		comment: 'went well',
		result: ['5fb3e17004db82000118ffd6', '5fb3e18004db82000118ffd7'],
		resultTime: '2020-11-12T19:29:34.244Z',
		procedure: 'unknown',
		memberOf: '5fb3e10f04db82000118ffd4',
		soDepth: {
			minValue: { value: 0, unit: 'unit:CM' },
			maxValue: { value: 20, unit: 'unit:CM' },
		},
	},
	{
		id: '5fb42b8f04db82000118ffda',
		featureOfInterest: '5f9840bb6dc285fd63d96e4e',
		geometry: {
			type: 'Point',
			coordinates: [-84.49588990912653, 39.1186523358723],
		},
		property: { geoAccuracy: 150 },
		comment: '',
		result: ['5fb42b9604db82000118ffdb', '5fb42b9c04db82000118ffdc'],
		resultTime: '2020-11-12T19:29:34.244Z',
		procedure: 'unknown',
		memberOf: '5fb3e10f04db82000118ffd4',
		soDepth: {
			minValue: { value: 0, unit: 'unit:CM' },
			maxValue: { value: 30, unit: 'unit:CM' },
		},
	},
	{
		id: '5fb42bc30c26e20001149178',
		featureOfInterest: '5f9840bb6dc285fd63d96e53',
		geometry: {
			type: 'Point',
			coordinates: [-84.49588990912653, 39.1186523358723],
		},
		property: { geoAccuracy: 150 },
		comment: '',
		result: ['5fb42bca0c26e20001149179'],
		resultTime: '2020-11-12T19:29:34.244Z',
		procedure: 'unknown',
		memberOf: '5fb3e10f04db82000118ffd4',
		soDepth: {
			minValue: { value: 0, unit: 'unit:CM' },
			maxValue: { value: 30, unit: 'unit:CM' },
		},
	},
];

export const samples = [
	{
		id: '5fb3e17004db82000118ffd6',
		name: 'asdf',
		resultOf: '5fb3e14c04db82000118ffd5',
		soDepth: {
			minValue: { value: 0, unit: 'unit:CM' },
			maxValue: { value: 15, unit: 'unit:CM' },
		},
	},
	{
		id: '5fb3e18004db82000118ffd7',
		name: 'dddd',
		resultOf: '5fb3e14c04db82000118ffd5',
		soDepth: {
			minValue: { value: 10, unit: 'unit:CM' },
			maxValue: { value: 20, unit: 'unit:CM' },
		},
	},
	{
		id: '5fb42b9604db82000118ffdb',
		name: 'aaaa',
		resultOf: '5fb42b8f04db82000118ffda',
		soDepth: {
			minValue: { value: 0, unit: 'unit:CM' },
			maxValue: { value: 15, unit: 'unit:CM' },
		},
	},
	{
		id: '5fb42b9c04db82000118ffdc',
		name: 'bbbbb',
		resultOf: '5fb42b8f04db82000118ffda',
		soDepth: {
			minValue: { value: 15, unit: 'unit:CM' },
			maxValue: { value: 30, unit: 'unit:CM' },
		},
	},
	{
		id: '5fb42bca0c26e20001149179',
		name: 'cccc',
		resultOf: '5fb42bc30c26e20001149178',
		soDepth: {
			minValue: { value: 0, unit: 'unit:CM' },
			maxValue: { value: 15, unit: 'unit:CM' },
		},
	},
];

export const samplingCollection = {
	id: '5fb3e10f04db82000118ffd4',
	name: 'Sampling Collection',
	object: '5f9840bb6dc285fd63d96e59',
	featureOfInterest: '5f9840bb6dc285fd63d96e3d',
	member: [
		'5fb3e14c04db82000118ffd5',
		'5fb42b8f04db82000118ffda',
		'5fb42b8f04db82000118ffda',
		'5fb42bc30c26e20001149178',
	],
	soDepth: {
		minValue: { value: 0, unit: 'unit:CM' },
		maxValue: { value: 30, unit: 'unit:CM' },
	},
	sampleDepths: [
		{
			minValue: { value: 0, unit: 'unit:CM' },
			maxValue: { value: 15, unit: 'unit:CM' },
		},
		{
			minValue: { value: 15, unit: 'unit:CM' },
			maxValue: { value: 30, unit: 'unit:CM' },
		},
	],
	dateCreated: '2020-11-17T14:41:19.084Z',
	dateModified: '2020-11-17T20:00:12.002Z',
};
